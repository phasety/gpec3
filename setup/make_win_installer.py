#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import gpec
import os.path

version = gpec.__version__.replace(' ', '-')

def banner(msg):
    print ""
    print "*" * 79
    print msg.center(79)
    print "*" * 79
    print ""

banner("Running py2exe...")
here = os.path.dirname(__file__)
src = os.path.join(here, '..', 'src')
subprocess.call(['python', 'setup.py', 'py2exe'], cwd='src')

make_nsis = "C:\Archivos de programa\NSIS\makensis.exe"
version_param = '/DVERSION=%s' % version
script = os.path.join(here, 'setup.nsi')

banner("Running NSIS")
subprocess.call([make_nsis, version_param, script])

installer = "gpec-%s-setup.exe" % version

banner( "Ok!..Installer done as %s" % installer)

banner("Uploading to phasety.com ...")

subprocess.call(["scp.exe", installer, "phasety@phasety.com:phasety.com/tmp/uploads"])

banner("New installer uploaded. Attach it to the article...")

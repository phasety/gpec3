#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import wx

class FloatValidator(wx.PyValidator):
    CTRL_C, CTRL_V, CTRL_X = 3, 22, 24
    def __init__(self, pyVar = None):
        wx.PyValidator.__init__(self)
        self.charlist = [ str(n) for n in range(0, 10) ]
        self.charlist.append('.')
        self.charlist.append('-')
        self.codelist = [wx.WXK_SPACE, wx.WXK_DELETE,
                         wx.WXK_BACK, wx.WXK_LEFT, wx.WXK_RIGHT,
                         wx.WXK_TAB, self.CTRL_C, self.CTRL_V, self.CTRL_X]
        self.Bind(wx.EVT_CHAR, self.ProcessKey)

    def Clone(self):
        return FloatValidator()

    def ProcessKey(self, event):
        key = event.GetKeyCode()

        if key in self.codelist or (key in range(256) and chr(key) in self.charlist):
            event.Skip()
            return

        if not wx.Validator_IsSilent():
            wx.Bell()

    def Validate(self, win):
        textCtrl = self.GetWindow()
        text = textCtrl.GetValue()

        if len(text) == 0:
            textCtrl.SetBackgroundColour("pink")
            textCtrl.SetFocus()
            textCtrl.Refresh()
            return False
        else:
            textCtrl.SetBackgroundColour(
            wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW))
            textCtrl.Refresh()
            return True

    def TransferToWindow(self):
        return True

    def TransferFromWindow(self):
        return True


class NotEmptyValidator(wx.PyValidator):
     def __init__(self):
         wx.PyValidator.__init__(self)

     def Clone(self):
         """
         Note that every validator must implement the Clone() method.
         """
         return NotEmptyValidator()

     def Validate(self, win):
         textCtrl = self.GetWindow()
         text = textCtrl.GetValue()

         if len(text) == 0:
             wx.MessageBox("This field must contain some value", "Error")
             textCtrl.SetBackgroundColour("pink")
             textCtrl.SetFocus()
             textCtrl.Refresh()
             return False
         else:
             textCtrl.SetBackgroundColour(
                 wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW))
             textCtrl.Refresh()
             return True

     def TransferToWindow(self):
         return True

     def TransferFromWindow(self):
         return True

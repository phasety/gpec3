# -*- coding: utf-8 -*-

import wx
import wx.lib.customtreectrl
from wx.lib.pubsub import pub

class PlotsTreePanel(wx.Panel):
    """panel to show/hide plots pages"""

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)

        #TREE
        self.tree = wx.lib.customtreectrl.CustomTreeCtrl(self, -1, wx.DefaultPosition, wx.DefaultSize,
                               wx.TR_DEFAULT_STYLE | wx.TR_HIDE_ROOT |
                               wx.lib.customtreectrl.TR_AUTO_CHECK_CHILD |
                               wx.lib.customtreectrl.TR_AUTO_CHECK_PARENT )
        self.root = self.tree.AddRoot("")

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(self.tree, 1, wx.EXPAND)
        self.SetSizerAndFit(sizer)
        self.tree_data = {}
        self.pydata_inverse = {} #'panel_name':node

        pub.subscribe(self.AddCheckboxItem, 'add checkbox')
        pub.subscribe(self.UncheckItem, 'uncheck item')
        pub.subscribe(self.SelectItem, 'select item')

        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnTreeSelChanged, self.tree)
        self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.OnBeginDrag, self.tree)
        self.Bind(wx.EVT_TREE_DELETE_ITEM, self.OnDeleteItem, self.tree)
        self.Bind(wx.EVT_TREE_END_DRAG, self.OnEndDrag, self.tree)

    def OnBeginDrag(self, event):
        item = event.GetItem()
        if not self.tree.GetPyData(item):
            #just allowing when the item is associated to a plot and not to a group of plot.
            event.Veto()
            return
        self.draggedItem = item
        event.Allow()

    def OnEndDrag(self, event):
        droppedItem = event.GetItem()
        if droppedItem == self.draggedItem or not self.tree.GetPyData(droppedItem):
            # No way, we have been dropped in a dragged item
            # or dropping in a not plot-associated item
            event.Veto()

        pub.sendMessage('refresh all', None)
        pub.sendMessage('make_custom', (self.tree.GetPyData(self.draggedItem),
                                            self.tree.GetPyData(droppedItem) ) )

    def SelectItem(self, message):
        panel_name = message.data
        try:
            node = self.pydata_inverse[panel_name]
            self.tree.SelectItem(node, True)
        except KeyError:
            print "key Error"

    def UncheckItem(self, message):
        panel_name = message.data
        try:
            node = self.pydata_inverse[panel_name]
            self.tree.CheckItem(node, False)
        except KeyError:
            print "key Error"

    def OnTreeSelChanged(self, event):
        item = event.GetItem()
        panel_name = self.tree.GetPyData(item)
        checked = self.tree.IsItemChecked(item)
        if panel_name and checked:
            pub.sendMessage('active page', panel_name)

    def AddCheckboxItem(self, message):
        """
        Case X
         |__ 2D
         |  |__ Global
         |  |   |___ PT
         |  |   |___ Tx
         |  |   |___ ...
         |  |
         |  |__ Isop (Z=z1)
         |  |   |__ ...
         |  |   |__ ...
         |  |
         |  |__ Txy (P=p1)
         |  |   |__
         |  |   |__
         |  |
         |  |__ Pxy (T=z1)
         |      |__ ...
         |      |__ ...
         |
         |__ 3D
            |__ PTrho
            |   |___ PT         #TODO
            |   |___ Isop
            |   |___ Txy
            |
            |
            |__ PTx
               |___ PT
               |___ Tx
               |___ ...

        Custom Plots
           |
           |_ Custom plot 1
           |
           |_ Custom plot 2
        """

        category_translate = {'globalsuite': 'Global Phase',
                              'globalsuite3d': '',
                              'isop': 'Isopleths', 'txy': 'Txy',
                               'pxy': 'Pxy', 'custom': ''
        }

        if len(message.data) == 4:
            case_id, topic, title, panel_name = message.data
            category = category_translate[topic] #globalsuite, globalsuite3d
        elif len(message.data) == 5:
            case_id, topic, title, panel_name, extra_var = message.data
            category = category_translate[topic] + " " + extra_var

        if case_id is None and 'custom' not in self.tree_data.keys():
            node = self.tree.AppendItem(self.root, "Custom plots", ct_type=1)
            self.tree_data['custom'] = {'node': node, 'childs': {}}

        elif case_id not in self.tree_data.keys() and case_id is not None:
            node = self.tree.AppendItem(self.root, "Case %d" % case_id, ct_type=1)
            node2d = self.tree.AppendItem(node, "2D", ct_type=1)
            node3d = self.tree.AppendItem(node, "3D", ct_type=1)
            self.tree_data[case_id] = {'node': node,
                                        'childs': {'2D': {'node': node2d,
                                                          'childs': {} },
                                                   '3D': {'node': node3d,
                                                          'childs': {} },
                                                   }
                                      }
            self.tree.CheckItem2(node, True)
            self.tree.CheckChilds(node, True)
            self.tree.ExpandAllChildren(node)

        if topic not in ('globalsuite3d', 'custom') and \
            category not in self.tree_data[case_id]['childs']['2D']['childs'].keys():

            parent_node = self.tree_data[case_id]['childs']['2D']['node']
            node = self.tree.AppendItem(parent_node, category, ct_type=1)
            self.tree_data[case_id]['childs']['2D']['childs'][category] = {'node': node, 'childs': {} }
            self.tree.CheckItem2(node, True)
            self.tree.Expand(parent_node)

        if topic == 'globalsuite3d':
            parent_node = self.tree_data[case_id]['childs']['3D']['node']
        elif topic == 'custom':
            parent_node = self.tree_data['custom']['node']
        else:
            parent_node = self.tree_data[case_id]['childs']['2D']['childs'][category]['node']

        #append the node
        node = self.tree.AppendItem(parent_node, title, ct_type=1)
        self.tree.Expand(parent_node)
        self.tree.SetPyData(node, panel_name)
        self.pydata_inverse[panel_name] = node

        self.tree.CheckItem2(node)
        #self.tree.CheckItem(node)
        self.Bind(wx.lib.customtreectrl.EVT_TREE_ITEM_CHECKED,
                  self.OnItemChecked, self.tree)

        #select just generated plot
        panel_name = self.tree.GetPyData(node)
        pub.sendMessage('active page', panel_name)



    def OnDeleteItem(self, event):
        def remove_panel_name (item):
            self.tree.GetPyData(item)
            panel_name = get_panel_name(item)

            if panel_name:
                pub.sendMessage('delete page', panel_name)
                self.tree.Delete(item)
            else:
                (child, cookie) = self.tree.GetFirstChild(item)
                while child:
                    remove_panel_name(child)
                    (child, cookie) = self.tree.GetNextChild(item, cookie)
        item = event.GetItem()
        remove_panel_name (item)


    def OnItemChecked(self, event):
        item = event.GetItem()
        panel_name = self.tree.GetPyData(item)
        checked = self.tree.IsItemChecked(item)
        if panel_name: # means it's a child
            if checked:
                pub.sendMessage('show page', panel_name)
            else:
                pub.sendMessage('hide page', panel_name)
        else:
            (child, cookie) = self.tree.GetFirstChild(item)
            while child:
                self.tree.CheckItem(child, checked)      #recursiveness
                (child, cookie) = self.tree.GetNextChild(item, cookie)

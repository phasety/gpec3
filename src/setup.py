try:
    from setuptools import setup
    #from distutils.core import setup
except ImportError:
    import distribute_setup
    distribute_setup.use_setuptools()
    from setuptools import setup
try:
    import py2exe
except ImportError:
    #no windows platform
    pass

import sys

if not sys.platform.startswith('win'):
    try:
        import subprocess
        subprocess.call(['wine'])
    except OSError:
        print('You need wine to install GPEC on your system.')
        sys.exit(1)

import os
import glob

def find_data_files(source,target,patterns):
    """Locates the specified data-files and returns the matches
    in a data_files compatible format.

    source is the root of the source data tree.
        Use '' or '.' for current directory.
    target is the root of the target data tree.
        Use '' or '.' for the distribution directory.
    patterns is a sequence of glob-patterns for the
        files you want to copy.
    """
    if glob.has_magic(source) or glob.has_magic(target):
        raise ValueError("Magic not allowed in src, target")
    ret = {}
    for pattern in patterns:
        pattern = os.path.join(source,pattern)
        for filename in glob.glob(pattern):
            if os.path.isfile(filename):
                targetpath = os.path.join(target,os.path.relpath(filename,source))
                path = os.path.dirname(targetpath)
                ret.setdefault(path,[]).append(filename)
    return sorted(ret.items())

import matplotlib
from gpec import __version__, __author__

data_mpl = matplotlib.get_py2exe_datafiles()

includes = ['numpy.core.umath']
excludes = ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter']
packages = ['wx.lib.pubsub', 'gpec']
dll_excludes = ['libgdk-win32-2.0-0.dll', 'libgobject-2.0-0.dll', 'tcl84.dll',
                'tk84.dll']


with open('README.txt') as readme:
    long_description =  readme.read()

setup(
    name = 'GPEC',
    version = __version__,
    description = u'Calculates and plots global phase equilibriums and other '\
                  u'thermodynamics diagrams for binary systems',
    long_description = long_description,
    author = " & ".join(__author__),
    author_email = 'gaitan@gmail.com',
    url = 'http://gpec.phasety.com',
    install_requires = ['matplotlib'], # 'wx'],
    data_files = data_mpl + find_data_files('data', 'data',
                                            ['icons/*.png', 'icons/log/*.png',
                                             'db/*', 'backend/*.exe',
                                             'LICENSE.txt']),

    options = {"py2exe": {"compressed": 2,
                          "optimize": 0,
                          "includes": includes,
                          "excludes": excludes,
                          "packages": packages,
                          "dll_excludes": dll_excludes,
                          "bundle_files": 3,
                          "dist_dir": "dist",
                          "xref": False,
                          "skip_archive": False,
                          "ascii": False,
                          "custom_boot_script": '',
                         }
              },
    windows = [
            {
            "script": "bin/gpec",
            "icon_resources": [(1, "data/icons/gpec.ico")]
            }
        ],
    scripts = ["bin/gpec"],
)

SetCompressor /solid lzma
XPStyle on

!include EnvVarUpdate.nsh

#Page license
Page directory
Page instfiles

RequestExecutionLevel admin

# set license page
#LicenseText ""
#LicenseData "LICENSE.txt"
#LicenseForceSelection checkbox

Name "GPEC ${VERSION}"
OutFile "gpec-${VERSION}-setup.exe"

InstallDir "$PROGRAMFILES\GPEC-${VERSION}"
InstallDirRegKey HKLM "Software\GPEC" "Install_Dir"

Section "Install"
    SectionIn RO
    SetOutPath $INSTDIR
    File /r ..\src\dist\*.*
    WriteRegStr HKLM "Software\GPEC-${VERSION}" "Install_Dir" "$INSTDIR"
    WriteUninstaller "$INSTDIR\uninstall.exe"
SectionEnd

Section "Shortcuts"
    CreateDirectory "$SMPROGRAMS\GPEC"
    CreateShortCut "$SMPROGRAMS\GPEC\GPEC.lnk" "$INSTDIR\gpec.exe" "" "$INSTDIR\gpec.exe"
    CreateShortCut "$SMPROGRAMS\GPEC\uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe"
    ;${lnkX64IconFix} "$SMPROGRAMS\GPEC\uninstall.lnk"
    CreateShortCut "$DESKTOP\GPEC.lnk" "$INSTDIR\gpec.exe" "" "$INSTDIR\gpec.exe"
    ;${lnkX64IconFix} "$DESKTOP\GPEC.lnk"
SectionEnd

Section "Uninstall"
    Delete "$INSTDIR\uninstall.exe"
    RMDir /r $INSTDIR
    RMDir /r "$PROFILE\GPEC\"
    RMDir /r "$SMPROGRAMS\GPEC"
    Delete "$DESKTOP\GPEC.lnk"
    DeleteRegKey HKLM "Software\GPEC"
SectionEnd

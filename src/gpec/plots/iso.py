# -*- coding: utf-8 -*-
from wx.lib.pubsub import pub
from base import Plot2D


class IsoPT(Plot2D):
    """Isopleth PT diagram"""

    def __init__(self, parent, arrays=None, **kwargs):
        self.short_title = u"Isopleth P-T"
        self.title = u'P-T projection of the Isopleth Graph '
        if 'system' in kwargs and 'z' in kwargs:

            self.title += ("for a molar fraction of %s equal to %s" %
                           (kwargs.get('system', [''])[0], kwargs.get('z', '')))

        self.xlabel = u'Temperature [K]'
        self.ylabel = u'Pressure [bar]'
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel,
                        self.ylabel, **kwargs)

    def setup_curves(self, arrays):
        if 'CRI' in arrays.keys():
            name = u'Critical point'
            try:
                cr_point = arrays['CRI'][0]
                self.curves.append({'_is_a_point': True,
                                    'name': 'Critical Points',
                                    'visible': True,
                                    'lines': (cr_point[0], cr_point[1]),    # TODO
                                    'marker': 'o',
                                    'color': 'black',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'CRI',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))
        if 'ISO' in arrays.keys():
            lines = []
            name = u'Isopleth lines'
            try:
                for num, vap_curve in enumerate(arrays['ISO']):
                    label = name if num == 0 else '_nolegend_'
#                   label = name   # Estas lineas tambien quedan para el bug de abajo
                    lines += self.axes.plot(vap_curve.T[0], vap_curve.T[1],
                                            'g', label=label)

#   Dejo estas lineas horribles, para eventualmente mostrar graficamente que
#   hay un bug menor aqui: se imprime una linea de mas (la primera que se pasa)
#      if num == 0: lines += self.axes.plot(vap_curve[:,0], vap_curve[:,1],
#                                           'y+', label=label)
#      if num == 1: lines += self.axes.plot(vap_curve[:,0], vap_curve[:,1],
#                                           'k', label=label)
#      else: lines += self.axes.plot(vap_curve[:,0], vap_curve[:,1], '+c', label=label)

                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'violet',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))
        if 'LLV' in arrays.keys():
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    if llv_curve.shape == (2,):
                        #POINT
                        (self.curves.append({'_is_a_point': True,
                                             'name': 'LLV Point',
                                             'visible': True,
                                             'lines': (llv_curve[0], llv_curve[1]),
                                             'marker': '^',
                                             'color': 'blue',
                                             'wx_id': self.get_unique_GUI_id(),
                                             'type': 'LLV',
                                             }))
                    else:
                        color = 'red'
                        name = 'LLV'
                        self.curves.append({'name': name,
                                            'visible': True,
                                            'lines': (llv_curve.T[0], llv_curve.T[1]),
                                            'color': color,
                                            'wx_id': self.get_unique_GUI_id(),
                                            'type': 'LLV',
                                            })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))


class IsoTx(Plot2D):
    """Isopleth Tx diagram"""

    def __init__(self, parent, arrays=None, **kwarg):
        comp0 = kwarg.get('system', [''])[0]
        self.short_title = u"Isopleth T-x"
        self.title = u'T-x projection of the Isopleth Graph for a molar '\
                     u'fraction of %s equal to %s' % \
                     (comp0, kwarg.get('z', ''))
        self.xlabel = u'Composition' if not comp0 else "%s molar fraction" % comp0
        self.ylabel = u'Temperature [K]'
        Plot2D.__init__(self, parent, arrays, self.title,
                        self.xlabel, self.ylabel, **kwarg)

    def setup_curves(self, arrays):
        if 'ISO' in arrays.keys():
            lines_g = []
            lines_r = []
            name_g = u'Incipient phase'
            name_r = u'Saturated phase'
            try:
                for num, iso_curve in enumerate(arrays['ISO']):
                    label_g = name_g if num == 0 else '_nolegend_'
                    label_r = name_r if num == 0 else '_nolegend_'
                    x = 1 - iso_curve.T[3]
                    t = iso_curve.T[0]
                    lines_g += self.axes.plot(x, t, 'k', label=label_g)

                    x_sat = (x[0], x[0])
                    t_sat = (max(t), min(t))
                    lines_r += self.axes.plot(x_sat, t_sat, 'g', label=label_r)

                self.curves.append( {'name': name_g,
                                         'visible': True,
                                         'lines2d': lines_g,
                                          'color' : 'green',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                            } )

                self.curves.append( {'name': name_r,
                                         'visible':True,
                                         'lines2d': lines_r,
                                          'color' : 'green',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                            } )

                cep = self.axes.plot(x[0], t[0], 'ok', label='__nolegend__')
                self.curves.append( { 'name': 'Critical Point',
                                      'visible': True,
                                     'lines2d': cep,
                                      'color' : 'black',
                                      'wx_id' : self.get_unique_GUI_id(),
                                      'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))


class IsoPx(Plot2D):
    """Isopleth Px diagram"""

    def __init__(self, parent, arrays=None, **kwarg):
        comp0 = kwarg.get('system', [''])[0]
        self.short_title = u"Isopleth P-x"
        self.title = u'P-x projection of the Isopleth Graph for a molar '\
                     u'fraction of %s equal to %s' % \
                     (comp0, kwarg.get('z', ''))
        self.xlabel = u'Composition' if not comp0 else "%s molar fraction" % comp0
        self.ylabel = u'Pressure [bar]'

        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, **kwarg)

    def setup_curves(self, arrays):

        if 'ISO' in arrays.keys():
            lines_g = []
            lines_r = []
            name_g = u'Incipient phase'
            name_r = u'Saturated phase'
            try:
                for num, iso_curve in enumerate(arrays['ISO']):
                    label_g = name_g if num == 0 else '_nolegend_'
                    label_r = name_r if num == 0 else '_nolegend_'

                    x = 1 - iso_curve.T[3]
                    p = iso_curve.T[1]
                    lines_g += self.axes.plot(x, p, 'k', label=label_g)

                    x_sat = (x[0], x[0])
                    p_sat = (max(p), min(p))
                    lines_r += self.axes.plot(x_sat, p_sat, 'g', label=label_r)

                pub.sendMessage('register', ('lines_r', lines_r))

                self.curves.append({'name': name_g,
                                    'visible': True,
                                    'lines2d': lines_g,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })

                self.curves.append( {'name': name_r,
                                         'visible':True,
                                         'lines2d': lines_r,
                                          'color' : 'red',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                            } )

                cep = self.axes.plot(x[0], p[0], 'ok', label='__nolegend__')
                self.curves.append( { 'name': 'Critical Point',
                                      'visible': True,
                                     'lines2d': cep,
                                      'color' : 'black',
                                      'wx_id' : self.get_unique_GUI_id(),
                                      'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))


class IsoTrho(Plot2D):
    """Isopleth Trho diagram"""

    def __init__(self, parent, arrays=None, **kwarg):
        self.short_title = u"Isopleth T-\u03c1"
        self.title = u'T-\u03c1 of the Isopleth Graph for a molar '\
                     u'fraction of %s equal to %s' % \
                     (kwarg.get('system', [''])[0], kwarg.get('z', ''))
        self.xlabel = u'Density [mol/l]'
        self.ylabel = u'Temperature [K]'
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, **kwarg)

    def setup_curves(self, arrays):

        if 'ISO' in arrays.keys():
            lines_incip = []
            lines_satur = []

            name_g = u'Incipient phase'
            name_r = u'Saturated phase'

            try:
                for num, vap_curve in enumerate(arrays['ISO']):
                    label_g = name_g if num == 0 else '_nolegend_'
                    label_r = name_r if num == 0 else '_nolegend_'

                    cols = (4, 5) if num != 3 else (5, 4)       # FIX ME
                    lines_incip += self.axes.plot(vap_curve[:,cols[0]],
                                                   vap_curve[:,0],
                                                   'k',
                                                   label=label_g)
                    lines_satur +=  self.axes.plot(vap_curve[:,cols[1]],
                                                   vap_curve[:,0],
                                                   'g',
                                                   label=label_r)

                self.curves.append( {'name': name_g,
                                         'visible':True,
                                         'lines2d': lines_incip,
                                          'color' : 'green',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                            } )
                self.curves.append( {'name': name_r,
                                         'visible':True,
                                         'lines2d': lines_satur,
                                          'color' : 'black',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                        } )

            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))


class IsoPrho(Plot2D):
    """Isopleth Prho diagram"""

    def __init__(self, parent, arrays=None, **kwarg):
        comp0 = kwarg.get('system', [''])[0]
        self.short_title = u"Isopleth P-\u03c1"
        self.title = u'P-\u03c1 of the Isopleth Graph for a molar '\
                     u'fraction of %s equal to %s' % (comp0, kwarg.get('z', ''))
        kwarg.get('z', '')
        self.xlabel = u'Density [mol/l]'
        self.ylabel = u'Pressure [bar]'

        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, **kwarg)

    def setup_curves(self, arrays):

        if 'ISO' in arrays.keys():
            lines_incip = []
            lines_satur = []

            name_g = u'Incipient phase'
            name_r = u'Saturated phase'

            try:
                for num, vap_curve in enumerate(arrays['ISO']):
                    label_g = name_g if num == 0 else '_nolegend_'
                    label_r = name_r if num == 0 else '_nolegend_'
                    try:
                        lines_incip +=  self.axes.plot(vap_curve[:,4],
                                                       vap_curve[:,1],
                                                       'k', label=label_g)
                        lines_satur +=  self.axes.plot(vap_curve[:,5],
                                                       vap_curve[:,1],
                                                       'g', label=label_r)
                    except IndexError:
                        print num

                self.curves.append( {'name': name_g,
                                         'visible':True,
                                         'lines2d': lines_incip,
                                          'color' : 'green',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                            } )
                self.curves.append( {'name': name_r,
                                         'visible':True,
                                         'lines2d': lines_satur,
                                          'color' : 'black',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'ISO',
                                        } )

                cep = self.axes.plot(vap_curve[:,4][0], vap_curve[:,1][0], 'ok', label='__nolegend__')
                self.curves.append( { 'name': 'Critical Point',
                                      'visible': True,
                                     'lines2d': cep,
                                      'color' : 'black',
                                      'wx_id' : self.get_unique_GUI_id(),
                                      'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in ISOPOUT.DAT'))

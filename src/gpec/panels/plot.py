# -*- coding: utf-8 -*-

import wx
import time
import wx.lib.agw.aui as aui
if __name__ == "__main__":
    from wx.lib.pubsub import setuparg1
from wx.lib.pubsub import pub

import matplotlib
matplotlib.use('WXAgg')
from matplotlib.backends.backend_wxagg import (NavigationToolbar2Wx
                                               as NavigationToolbar)

from gpec.settings import PLOT_IN_3D, PLOT_SUITES
from gpec import plots


class SuitePlotsPanel(wx.Panel):
    """a general tabbed panel to show plots"""

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        self.nb = aui.AuiNotebook(self, style=aui.AUI_NB_DEFAULT_STYLE)

        sizer = wx.BoxSizer()
        sizer.Add(self.nb, 1, wx.EXPAND)
        self.SetSizerAndFit(sizer)
        self.done = True
        pub.subscribe(self.MakePlots, 'make')  # for all kind of suites
        pub.subscribe(self.MakeCustomPlot, 'make_custom')  # for all kind
        pub.subscribe(self.HidePage, 'hide page')  # from checkbox tree
        pub.subscribe(self.ShowPage, 'show page')  # from checkbox tree
        pub.subscribe(self.DeletePage, 'delete page')  # from checkbox tree
        pub.subscribe(self.ActivePage, 'active page')  # whenitem is selected

        self.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.OnPageClosing, self.nb)
        self.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnPageChange, self.nb)
        self.hidden_page = {}

        # plots in 3d are uniques for each cases.
        # Every new data is drawn on the same plot
        # plots in 3d are uniques for each cases.
        # Every new data is drawn on the same plot
        self.plot3d_instances = {}
        # unique ID to keep plots generated with the same click grouped
        self.suite_counter = 0
        self.custom_plot_counter = 0

    def OnPageClosing(self, event):
        """
        handler aui.EVT_AUINOTEBOOK_PAGE_CLOSE. This rely on
        UncheckItem on PlotTreePanel instance so the mechanism
        to backup the content of the page is used.
        """
        page_id = event.selection
        panel_name = self.nb.GetPage(page_id).GetName()
        pub.sendMessage('uncheck item', panel_name)
        event.Veto()

    def OnPageChange(self, event):
        """
        handler aui.EVT_AUINOTEBOOK_PAGE_CLOSE.
        This rely on UncheckItem on PlotTreePanel instance
        so the mechanism to backup the content of the page is used.
        """
        page_id = event.selection
        panel_name = self.nb.GetPage(page_id).GetName()
        pub.sendMessage('select item', panel_name)
        #event.Veto()

    def ActivePage(self, message):
        """
        handler for the topic 'active page' produced on PlotsTreePanel when
        the user select an item from the tree which is associated with a
        notebook page.
        """

        window = wx.FindWindowByName(message.data)
        page_id = self.nb.GetPageIndex(window)
        if page_id is not None:
            # TO DO debbug this crap
            try:
                self.nb.SetSelection(page_id)
            except:
                pass

    def HidePage(self, message):
        """close page which window is named as message.data
        and put in hidden_page"""

        window = wx.FindWindowByName(message.data)
        page_id = self.nb.GetPageIndex(window)
        self.hidden_page[message.data] = (window, self.nb.GetPageText(page_id))
        self.nb.RemovePage(page_id)
        window.Hide()

    def ShowPage(self, message):
        try:
            window, caption = self.hidden_page.pop(message.data)
            window.Show()
            self.nb.AddPage(window, caption)
        except KeyError:
            print "key  error ", message.topic, message.data

    def DeletePage(self, message):
        """remove a plot page including its plot panel """

        window = wx.FindWindowByName(message.data)
        page_id = self.nb.GetPageIndex(window)

        if message.data in self.hidden_page:
            self.hidden_page.pop(message.data)

        pub.sendMessage('remove_plot_instance', message.data)

        try:
            self.nb.RemovePage(page_id)
            window.Destroy()        # TO DO check this silent !
        except:
            pass

    def MakePlots(self, message):
        case_id, case_name, system = (list(message.data[0:2]) +
                                      [message.data[-1]])
        #3D
        if (not case_id in self.plot3d_instances and
                PLOT_IN_3D and self.done):
            self.plot3d_instances[case_id] = []
            plots3d_types = ['PTx', 'PTrho']
            for type in plots3d_types:
                panel_name = 'case_%i_%s' % (case_id, type)
                if type == plots3d_types[0]:
                    # on global suites, PTx is the default selected
                    panel_selected = panel_name
                pp3d = PlotPanel(self, -1, type,
                                 name=panel_name, system=system)
                self.plot3d_instances[case_id] += [pp3d]
                self.nb.AddPage(pp3d, "%s (%s)" % (pp3d.plot.short_title,
                                                   case_name))
                pub.sendMessage('add checkbox', (case_id, 'globalsuite3d',
                                                 pp3d.plot.short_title,
                                                 panel_name))

        #2D
        arrays = None
        type_suite = message.topic[1]
        if type_suite == 'globalsuite':
            case_id, case_name, arrays, system = message.data

            for type in PLOT_SUITES[type_suite]:
                panel_name = 'case_%i_suite_%i_%s' % (case_id,
                                                      self.suite_counter,
                                                      type)
                # name is useful to find the page later.
                pp = PlotPanel(self, -1, type, arrays,
                               name=panel_name, system=system)
                pub.sendMessage('add checkbox', (case_id, type_suite,
                                                 pp.plot.short_title,
                                                 panel_name))
                self.nb.AddPage(pp, "%s (%s)" % (pp.plot.short_title,
                                                 case_name))
                pp.Plot()

        elif type_suite == 'isop':
            case_id, case_name, arrays, z_val, system = message.data

            if arrays:
                for type in PLOT_SUITES[type_suite]:
                    tmpl = 'case_%i_suite_%i_%s_z_%s'
                    panel_name = tmpl % (case_id,
                                         self.suite_counter,
                                         type, z_val)
                    if type == PLOT_SUITES[type_suite][0]:
                        panel_selected = panel_name
                    pp = PlotPanel(self, -1, type, arrays,
                                   z=z_val, name=panel_name, system=system)
                    pub.sendMessage('add checkbox', (case_id, type_suite,
                                                     pp.plot.short_title,
                                                     panel_name,
                                                     '(Z = %s)' % z_val))
                    self.nb.AddPage(pp, "%s (%s)" % (pp.plot.short_title,
                                                     case_name))
                    pp.Plot()
            else:
                msg = ("Couldn't calculate for the given molar fraction (%s)"
                       % z_val)
                pub.sendMessage('log', ('error', msg))

        elif type_suite == 'pxy':
            case_id, case_name, arrays, t_val, system = message.data

            if arrays:
                for type in PLOT_SUITES[type_suite]:
                    tmpl = 'case_%i_suite_%i_%s_t_%s'
                    panel_name = tmpl % (case_id,
                                         self.suite_counter,
                                         type, t_val)
                    if type == PLOT_SUITES[type_suite][0]:
                        panel_selected = panel_name
                    pp = PlotPanel(self, -1, type, arrays,
                                   t=t_val, name=panel_name, system=system)
                    self.nb.AddPage(pp, "%s (%s)" % (pp.plot.short_title,
                                                     case_name))

                    pub.sendMessage('add checkbox', (case_id, type_suite,
                                                     pp.plot.short_title,
                                                     panel_name,
                                                     '(T = %s K)' % t_val))

                    pp.Plot()
            else:
                msg = ("Couldn't calculate for the given temperature (%s K)"
                       % t_val)
                pub.sendMessage('log', ('error', msg))

        elif type_suite == 'txy':
            case_id, case_name, arrays, p_val, system = message.data
            if arrays:
                for type in PLOT_SUITES[type_suite]:
                    tmpl = 'case_%i_suite_%i_%s_p_%s'
                    panel_name = tmpl % (case_id,
                                         self.suite_counter,
                                         type, p_val)
                    if type == PLOT_SUITES[type_suite][0]:
                        panel_selected = panel_name
                    pp = PlotPanel(self, -1, type,
                                   arrays, p=p_val,
                                   name=panel_name, system=system)
                    self.nb.AddPage(pp, "%s (%s)" % (pp.plot.short_title,
                                                     case_name))
                    pub.sendMessage('add checkbox', (case_id, type_suite,
                                                     pp.plot.short_title,
                                                     panel_name,
                                                     '(P = %s bar)' % p_val))
                    pp.Plot()
            else:
                msg = ("Couldn't calculate for the given pressure (%s bar)"
                       % p_val)
                pub.sendMessage('log', ('error', msg))

        #plot whatever on 3D diagrams.

        if PLOT_IN_3D and arrays:
            for pp3d in self.plot3d_instances[case_id]:
                kwarg = dict([(extra_var, float(locals()[extra_var]))
                             for extra_var in ['z_val', 't_val', 'p_val']
                             if extra_var in locals()])

                pp3d.plot.setup_curves(arrays, **kwarg)
                pp3d.Plot()

        self.suite_counter += 1
        if panel_selected:
            pub.sendMessage('select item', panel_selected)

    def MakeCustomPlot(self, message):

        plot_from, plot_to = [wx.FindWindowByName(panel_name).plot
                              for panel_name in message.data]

        if plot_from.projection == '2d':
            x_from, x_to = [ax.get_xlabel()[-4:]
                            for ax in (plot_from.axes, plot_to.axes)]
            y_from, y_to = [ax.get_ylabel()[-4:]
                            for ax in (plot_from.axes, plot_to.axes)]
            z_from, z_to = [None, None]
        else:
            #Matplotlib: why the hell there is no a get_zlabel method?
            x_from, x_to = [ax.w_xaxis.get_label_text()[-4:]
                            for ax in (plot_from.axes, plot_to.axes)]
            y_from, y_to = [ax.w_yaxis.get_label_text()[-4:]
                            for ax in (plot_from.axes, plot_to.axes)]
            z_from, z_to = [ax.w_zaxis.get_label_text()[-4:]
                            for ax in (plot_from.axes, plot_to.axes)]

        #check plot axis compatibilty
        if x_from != x_to or y_from != y_to or z_from != z_to:
            msg = ('The axis of the plots you are trying to '
                   'combine are differents')
            pub.sendMessage('log', ('error', msg))
            return

        self.custom_plot_counter += 1

        kwarg = {}
        kwarg['title'] = u"Custom plot %s" % self.custom_plot_counter

        kwarg['name'] = ("_".join(message.data) +
                         "_%s" % self.custom_plot_counter)

        if plot_from.projection == '3d':
            kwarg['projection'] = '3d'
            # TODO on PTx composition could be for different compounds
            kwarg['zlabel'] = 'Molar fraction of compound 1'
            kwarg['xlabel'] = plot_from.axes.w_xaxis.get_label_text()
            kwarg['ylabel'] = plot_from.axes.w_yaxis.get_label_text()
        else:
            kwarg['xlabel'] = plot_from.axes.get_xlabel()
            kwarg['ylabel'] = plot_from.axes.get_ylabel()

        pp = PlotPanel(self, -1, 'CustomPlot', **kwarg)
        pp.plot.setup_curves(plot_from.curves, plot_to.curves)

        self.nb.AddPage(pp, kwarg['title'])
        pub.sendMessage('add checkbox', (None, 'custom', kwarg['title'],
                                         kwarg['name']))


class PlotPanel(wx.Panel):
    """ Creates the main panel with all the controls on it:
             * mpl canvas
             * mpl navigation toolbar
             * Control panel for interaction"""

    def __init__(self, parent, id, diagram_type='PT', arrays=None, **kwarg):

        wx.Panel.__init__(self, parent, id,
                          style=wx.FULL_REPAINT_ON_RESIZE,
                          name=kwarg.pop('name', ''))
        self.parent = parent
        # any type of diagram
        self.plot = getattr(plots, diagram_type)(self, arrays=arrays, **kwarg)
        # binding matplotlib event
        self.plot.canvas.mpl_connect('button_release_event',
                                     self.onMouseButtonClick)
        self.plot.canvas.mpl_connect('motion_notify_event',
                                     self.onMouseMotion)

        # the canvas from plot
        self.toolbar = NavigationToolbar(self.plot.canvas)
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.plot.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)

        self.SetSizer(self.vbox)
        self.vbox.Fit(self)

        if arrays:
            self.plot.setup_curves(arrays)

        #binding via pubsub
        #pub.subscribe(self.OnPlotPT, 'plot.PT')

        pub.sendMessage('add_plot_instance', (self.GetName(), self.plot))

    def onMouseMotion(self, event):
        if event.inaxes:
            pub.sendMessage('status',
                            'X=%g Y=%g' % (event.xdata, event.ydata))

    def onMouseButtonClick(self, event):
        if event.button == 3:

            if not hasattr(self, 'menu'):
                self.menu = wx.Menu()

                for curve in self.plot.curves:
                    if curve['name'] not in ['LLV Point', 'Critical Points']:
                        self.menu.Append(curve['wx_id'], curve['name'],
                                         kind=wx.ITEM_CHECK)
                        self.Bind(wx.EVT_MENU, self.plot.OnToggleCurve,
                                  id=curve['wx_id'])
                        self.menu.Check(curve['wx_id'], True)

                self.menu.AppendSeparator()

                for prop, wx_id in self.plot.properties_normal.iteritems():
                    self.menu.Append(wx_id, prop)
                    self.Bind(wx.EVT_MENU, self.plot.OnSetupProperty, id=wx_id)

                for prop, wx_id in self.plot.properties_check.iteritems():
                    item = self.menu.Append(wx_id, prop, kind=wx.ITEM_CHECK)
                    self.Bind(wx.EVT_MENU,
                              self.plot.OnToggleProperty, id=wx_id)
                    if prop == 'Legends':
                        # Lengend are showed by default. So show the check
                        if self.plot.axes.legend_ is None:
                            self.menu.Check(wx_id, False)
                        else:
                            item.Toggle()

            # Popup the menu.  If an item is selected then its handler
            # will be called before PopupMenu returns.
            wx.CallAfter(self.PopupMenu, self.menu)

    def Plot(self, event=None):

        self._SetSize()
        self.plot.plot()
        self._resizeflag = False

    def _onSize(self, event):
        self._resizeflag = True

    def _onIdle(self, evt):
        if self._resizeflag:
            self._resizeflag = False
            self._SetSize()

    def _SetSize(self):
        pixels = tuple(self.parent.GetClientSize())
        self.SetSize(pixels)
        self.plot.canvas.SetSize(pixels)
        size = (float(pixels[0]) / self.plot.fig.get_dpi(),
                float(pixels[1]) / self.plot.fig.get_dpi())
        self.plot.fig.set_size_inches(*size)

if __name__ == "__main__":
    #test purposes
    import os
    import sys
    from gpec.tools.misc import reverse_and_expand_dict

    from gpec.apimanager import ApiManager

    if len(sys.argv) not in (3, 4):

        print 'Usage:', __file__, 'graph_type', 'path', '[run_fortran=False]'
        graphs = []
        for v in PLOT_SUITES.values():
            graphs.extend(v)
        print 'Available graph: \n'
        print '\n'.join(graphs)

        sys.exit()
    plot = sys.argv[1]
    plot_family = reverse_and_expand_dict(PLOT_SUITES)[plot]
    api = ApiManager(overriden_path=os.path.abspath(sys.argv[2]))
    arrays = api.read_generic_output(plot_family,
                                     run_fortran=len(sys.argv) == 4)
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    frame = wx.Frame(None)
    panel = PlotPanel(frame, -1, diagram_type=plot, arrays=arrays,)
    frame.Centre()
    frame.Show(True)
    app.MainLoop()

from custom import CustomPlot
from plots3d import PTrho, PTx
from iso import IsoPT, IsoPrho, IsoPx, IsoTrho, IsoTx
from global2d import PT, Tx, Px, Prho, Trho
from txy import Txy, TxyTrho
from pxy import Pxy, PxyPrho

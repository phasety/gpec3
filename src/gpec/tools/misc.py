#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import subprocess
import hashlib
import pickle

import platform
import urllib
import json
from datetime import date
from appdirs import user_data_dir
import uuid
from gpec.settings import INV_EOS, STATS_URL
from gpec import __version__, __full_title__


cache = {}


def compute_key(function, args, kw):
    key = pickle.dumps((function.func_name, args, kw))
    return hashlib.sha1(key).hexdigest()


def memoize():
    def _memoize(function):
        def __memoize(*args, **kw):
            key = compute_key(function, args, kw)

            if key not in cache.keys():     # we have'nt it already?
                result = function(*args, **kw)
                cache[key] = result         # store a new one
            else:
                # TODO may be it's a better idea get a file and parse it again
                # cache the whole arrays.
                print 'found in cache'

            return cache[key]
        return __memoize
    return _memoize


class Counter():
    """a singleton (but reseteable) counter to index cases."""
    count = 0

    def __init__(self, init=0):
        self.__class__.count += 1

    def reset(self):
        self.__class__.count = 0

    def get_id(self):
        return self.__class__.count


def ch_val2pos(choice, value):
    """return the position for a given value for a wx.Choice window"""
    eos = INV_EOS[value]
    return choice.GetItems().index(eos)


class curry():
    """Taken from the Python Cookbook, this class provides an easy way to
    tie up a function with some default parameters and call it later.
    See http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/52549 for more.
    """
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.pending = args[:]
        self.kwargs = kwargs

    def __call__(self, *args, **kwargs):
        if kwargs and self.kwargs:
            kw = self.kwargs.copy()
            kw.update(kwargs)
        else:
            kw = kwargs or self.kwargs
        return self.func(*(self.pending + args), **kw)


def reverse_and_expand_dict(source):
    """
    given a dictionary, being its values possiblely iterables,
    return a flat dict reversed
    """
    from collections import Iterable
    output = {}
    for k, values in source.items():
        if isinstance(values, Iterable):
            for v in values:
                output[v] = k
        else:
            output[values] = k
    return output


def open_folder(path):

    if sys.platform == 'win32':
        os.startfile(path)
    elif sys.platform == 'darwin':
        subprocess.Popen(['open', '--', path])
    else:
        try:
            subprocess.Popen(['xdg-open', path])
        except OSError:
            import webbrowser
            webbrowser.open(path)


def get_uid():
    """
    return a persistent uid for the install instance
    """
    path = user_data_dir('Gpec', 'Phasety')
    if not os.path.exists(path):
        os.makedirs(path)

    data_path = os.path.join(path, 'data.ini')
    try:
        with open(data_path, 'r') as f:
            return f.read()
    except:
        uid = uuid.uuid1().get_hex()
        with open(data_path, 'w') as f:
            f.write(uid)
        return uid


def ping(page="/gpec"):
    """
    request STATS_URL to statitics purposes.
    with GET vars :
        'uid'
        'gpec_version',
        'plat_system',
        'plat_release',
        'plat_version',
        'plat_machine',
        'plat_processor'

    the json response is parsed in  the format of
    ``geneate_json_to_response_on_ping``

    """

    class AppURLopener(urllib.FancyURLopener):
        version = __full_title__

    context = {'uid': get_uid(),
               'gpec_version': __version__,
               'plat_system': platform.system(),
               'plat_release': platform.release(),
               'plat_version': platform.version(),
               'plat_machine': platform.machine(),
               'plat_processor': platform.processor()}

    urllib._urlopener = AppURLopener()
    response = urllib.urlopen(STATS_URL + page, urllib.urlencode(context))
    try:
        return json.loads(response.read())
    except:
        pass


def geneate_json_to_response_on_ping(path="gpec",
                                     url_download='http://gpec.phasety.com'):
    """create a file  file to upload on STATS_URL as response
    {'last_version': '3.2',
     'release_date': date (in iso format)
     'url_download': http://... ,
     }
     """
    data = {'last_version': __version__,
            'release_date': date.today().strftime('%b %d, %Y'),
            'url_download': url_download}

    json.dump(data, open(path, 'w'))
    return data

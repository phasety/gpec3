# -*- coding: utf-8 -*-

from base import Plot2D
from wx.lib.pubsub import pub

class PT(Plot2D):
    """pressure-temperature diagram"""

    def __init__(self, parent, arrays=None, system=(), **kwarg):

        self.short_title = u"P-T"
        self.title = u'Pressure-Temperature projection of a univariant phase equilibrium diagram'
        self.xlabel = u'Temperature [K]'
        self.ylabel = u'Pressure [bar]'
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, system, **kwarg)

    def setup_curves(self, arrays):
        if 'VAP' in arrays.keys():
            lines = []
            name = u'Pure compound vapor pressure lines'
            try:
                for num, vap_curve in enumerate(arrays['VAP']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot( vap_curve[:,0], vap_curve[:,1], 'g' , label = label)
                self.curves.append( {'name': name, # + counter ,
                                         'visible':True,
                                         #'lines':( vap_curve[:,0], vap_curve[:,1]),
                                          'lines2d': lines,
                                          'color' : 'green',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'VAP',
                                            } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(cri_curve[:,0],cri_curve[:,1], 'k', label=label)
                self.curves.append( {'name': name,
                                     'visible':True,
                                     #'lines':(cri_curve[:,0],cri_curve[:,1]),
                                     'lines2d': lines,
                                     'color' : 'black',
                                     'wx_id' : self.get_unique_GUI_id(),
                                     'type': 'CRI'
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'LLV' in arrays.keys():
            lines = []
            name = 'LLV'
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label = 'LLV lines' if num == 0 else  'Liquid in LLV'
                    style = 'r' if num == 0 else 'b'
                    lines += self.axes.plot(llv_curve[:,0], llv_curve[:,1], style, label=label)
                self.curves.append( { 'name': name,
                                          'visible':True,
                                          'lines2d': lines,
                                          'color': 'red',
                                          'wx_id' : self.get_unique_GUI_id(),
                                           'type': 'LLV',
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'AZE' in arrays.keys():
            lines = []
            name = u'Azeotropic lines'
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot( aze_curve[:,0], aze_curve[:,1], 'm', label = label)
                self.curves.append( { 'name': name,
                                      'visible':True,
                                      'lines2d': lines,
                                      'color': 'magenta',
                                      'wx_id' : self.get_unique_GUI_id(),
                                      'type': 'LLV',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', 'There was an error trying to plot azeotropic lines. The diagram could not be accurate'))

class Tx(Plot2D):
    """T-x plot"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"T-x"
        self.title = u'Temperature-Composition projection of a univariant phase equilibrium diagram'
        self.xlabel = u'Composition' if not system else "%s molar fraction" % system[0]    #TODO DEFINE system inside the plot
        self.ylabel = u'Temperature [K]'
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, system, **kwargs)

    def setup_curves(self, arrays):
        #TODO VAP curves!
        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot (cri_curve[:,3], cri_curve[:,0], 'k', label = label)
                self.curves.append( {'name': name , # + counter,
                                     'visible':True,
                                     'lines2d': lines,
                                     'color' : 'black',
                                     'wx_id' : self.get_unique_GUI_id(),
                                     'type': 'CRI'
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'LLV' in arrays.keys():
            lines = []
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label_b = 'Liquid in LLV' if num == 0 else '_nolegend_'
                    label_r = 'Vapor in LLV' if num == 0 else '_nolegend_'
                    lines += self.axes.plot (llv_curve[:,2], llv_curve[:,0], 'b', label = label_b)
                    lines += self.axes.plot (llv_curve[:,3], llv_curve[:,0], 'b', label = '_nolegend_')
                    lines += self.axes.plot (llv_curve[:,4], llv_curve[:,0], 'r', label = label_r)
                self.curves.append( { 'name': 'LLV',
                                          'visible':True,
                                          'lines2d': lines,
                                          'color': 'blue',
                                          'wx_id' : self.get_unique_GUI_id(),
                                           'type': 'LLV',
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'AZE' in arrays.keys():
            name = u'Azeotropic line'
            lines = []
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot( aze_curve[:,2], aze_curve[:,0], 'm', label=label)
                self.curves.append( { 'name': name,
                                      'visible':True,
                                      'lines2d': lines,
                                      'color': 'magenta',
                                      'wx_id' : self.get_unique_GUI_id(),
                                       'type': 'LLV',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))

class Px(Plot2D):
    """P-x diagram"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"P-x"
        self.title = u'Pressure-Composition projection of a univariant phase equilibrium diagram'
        self.ylabel = u'Pressure [bar]'
        self.xlabel = u'Composition' if not system else "%s molar fraction" % system[0]    #TODO DEFINE system inside the plot
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, system, **kwargs)

    def setup_curves(self, arrays):
        #TODO VAP ?
        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot( cri_curve[:,3],cri_curve[:,1], 'k', label = label)
                    #counter = u'' if len(arrays['CRI']) == 1 else u' %i' % (num + 1)
                self.curves.append( {'name': name, # + counter,
                                         'visible':True,
                                         #'lines': tuple(lines),
                                         'lines2d': lines,
                                         'color' : 'black',
                                         'wx_id' : self.get_unique_GUI_id(),
                                         'type': 'CRI'
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'LLV' in arrays.keys():
            lines = []
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label_b = 'Liquid in LLV' if num == 0 else '_nolegend_'
                    label_r = 'Vapor in LLV' if num == 0 else '_nolegend_'
                    lines += self.axes.plot(llv_curve[:,2], llv_curve[:,1], 'b', label = label_b)
                    lines += self.axes.plot(llv_curve[:,3], llv_curve[:,1], 'b', label = '_nolegend_')
                    lines += self.axes.plot (llv_curve[:,4], llv_curve[:,1], 'r', label = label_r)
                self.curves.append( { 'name': 'LLV',
                                          'visible':True,
                                          'lines2d': lines,
                                          'color': 'red',
                                          'wx_id' : self.get_unique_GUI_id(),
                                           'type': 'LLV',
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'AZE' in arrays.keys():
            lines = []
            name = u'Azeotropic line'
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(aze_curve[:,2], aze_curve[:,1], 'm', label = label)
                self.curves.append( { 'name': name,
                                          'visible':True,
                                          'lines2d': lines,
                                          'color': 'magenta',
                                          'wx_id' : self.get_unique_GUI_id(),
                                           'type': 'LLV',
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))

class Trho(Plot2D):
    """temperature-density diagram"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u'T-\u03c1'
        self.title = u'Temperature-Density projection of a univariant phase equilibrium diagram'
        self.xlabel = u'Density [mol/l]'
        self.ylabel = u'Temperature [K]'
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, system, **kwargs)

    def setup_curves(self, arrays):
        if 'VAP' in arrays.keys():
            lines = []
            name = u'Pure compound vapor pressure lines'
            try:
                for num, vap_curve in enumerate(arrays['VAP']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot( vap_curve[:,2], vap_curve[:,0], 'g', label = label)
                    lines += self.axes.plot( vap_curve[:,3], vap_curve[:,0], 'g', label = '_nolegend_')
                self.curves.append( {'name': name,
                                         'visible':True,
                                          #'lines':(vap_curve[:,2], vap_curve[:,0], vap_curve[:,3], vap_curve[:,0]),
                                          'lines2d': lines,
                                          'color' : 'green',
                                          'wx_id' : self.get_unique_GUI_id(),
                                          'type': 'VAP',
                                            } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(cri_curve[:,2],cri_curve[:,0], 'k', label=label)
                    #counter = u'' if len(arrays['CRI']) == 1 else u' %i' % (num + 1)
                self.curves.append( {'name': name, # + counter ,
                                         'visible':True,
                                         #'lines':(cri_curve[:,2],cri_curve[:,0]),
                                         'lines2d': lines,
                                         'color' : 'black',
                                         'wx_id' : self.get_unique_GUI_id(),
                                         'type': 'CRI'
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'LLV' in arrays.keys():
            lines = []
            name = 'LLV'
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label_b = 'Liquid in LLV' if num == 0 else '_nolegend_'
                    label_r = 'Vapor in LLV' if num == 0 else '_nolegend_'
                    lines += self.axes.plot(llv_curve[:,7], llv_curve[:,0], 'b', label=label_b)
                    lines += self.axes.plot(llv_curve[:,8], llv_curve[:,0], 'b', label='_nolegend_')
                    lines += self.axes.plot (llv_curve[:,9], llv_curve[:,0], 'r', label = label_r)
                #TODO and the red curve?
                self.curves.append( { 'name': name,
                                      'visible':True,
                                      'lines2d': lines,           #TODO
                                      'color': 'blue',
                                      'wx_id' : self.get_unique_GUI_id(),
                                       'type': 'LLV',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'AZE' in arrays.keys():
            name = u'Azeotropic lines'
            lines = []
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot( aze_curve[:,4], aze_curve[:,0], 'm', label = label)
                    lines += self.axes.plot( aze_curve[:,5], aze_curve[:,0], 'm', label = '_nolegend_')
                self.curves.append( { 'name': name,
                                      'visible':True,
                                      'lines2d': lines,           #TODO
                                      'color': 'magenta',
                                      'wx_id' : self.get_unique_GUI_id(),
                                       'type': 'LLV',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))


class Prho(Plot2D):
    """pressure-density diagram"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u'P-\u03c1'
        self.title = u'Pressure-Density projection of a univariant phase equilibrium diagram'
        self.xlabel = u'Density [mol/l]'
        self.ylabel = u'Pressure [bar]'
        Plot2D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel, system, **kwargs)

    def setup_curves(self, arrays):
        if 'VAP' in arrays.keys():
            lines = []
            name = u'Pure compound vapor pressure lines'
            try:
                for num, vap_curve in enumerate(arrays['VAP']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(vap_curve[:,2], vap_curve[:,1], 'g', label=label)
                    lines += self.axes.plot(vap_curve[:,3], vap_curve[:,1], 'g', label='_nolegend_')
                    #counter = u'' if len(arrays['VAP']) == 1 else u' %i' % (num + 1)
                self.curves.append( {'name': name,
                                     'visible':True,
                                      'lines2d': lines,
                                      'color' : 'green',
                                      'wx_id' : self.get_unique_GUI_id(),
                                      'type': 'VAP',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(cri_curve[:,2],cri_curve[:,1], 'k', label=label)
                self.curves.append( {'name': name,
                                         'visible':True,
                                         'lines2d': lines,
                                         'color' : 'black',
                                         'wx_id' : self.get_unique_GUI_id(),
                                         'type': 'CRI'
                                        } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'LLV' in arrays.keys():
            lines = []
            name = 'LLV'
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label_b = 'Liquid in LLV' if num == 0 else '_nolegend_'
                    label_r = 'Vapor in LLV' if num == 0 else '_nolegend_'
                    lines += self.axes.plot(llv_curve[:,7], llv_curve[:,1], 'b', label=label_b)
                    lines += self.axes.plot(llv_curve[:,8], llv_curve[:,1], 'b', label='_nolegend_')
                    lines += self.axes.plot (llv_curve[:,9], llv_curve[:,1], 'r', label=label_r)
                self.curves.append( { 'name': name,
                                      'visible':True,
                                      'lines2d': lines,           #TODO
                                      'color': 'red',
                                      'wx_id' : self.get_unique_GUI_id(),
                                       'type': 'LLV',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))
        if 'AZE' in arrays.keys():
            lines = []
            name = u'Azeotropic lines'
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(aze_curve[:,4], aze_curve[:,1], 'm', label=label)
                    lines += self.axes.plot(aze_curve[:,5], aze_curve[:,1], 'm', label='_nolegend_')
                self.curves.append( { 'name': name,
                                      'visible':True,
                                      'lines2d': lines,
                                      'color': 'magenta',
                                      'wx_id' : self.get_unique_GUI_id(),
                                       'type': 'LLV',
                                    } )
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT'))


#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import wx
from wx.lib.wordwrap import wordwrap
from settings import data_dir
from gpec import __title__, __version__

class AboutBox():
    def __init__(self, parent):
        with open(os.path.join(data_dir, "LICENSE.txt" ), "r") as fh:
            license_text = fh.read()

        about_text = u"GPEC is a software that allows you to obtain phase diagrams and other thermodynamic plots for binary systems, as calculated with equations of state. It can be helpful for either educational, academic or development purposes. "\
                     u"It is easy to use and you do not have to provide any initial guesses or other inputs.\n\n "
                     

        info = wx.AboutDialogInfo()
        info.Name = __title__
        info.Version = __version__
        info.Copyright = u"(C) 2010-2013 Martín Cismondi & Martín Gaitán "
        info.Description = wordwrap(about_text, 350, wx.ClientDC(parent))
        info.WebSite = ("http://gpec.phasety.com", "Gpec Home Page")
        info.Developers = [u"Martín Cismondi Duarte",
                           u"Martín Gaitán",
                           u"Felipe Espósito",
                           u"Pablo Dalmasso"
                            ]

        info.License = wordwrap(license_text, 600, wx.ClientDC(parent))

        # Then we call wx.AboutBox giving it that info object
        wx.AboutBox(info)

if __name__ == '__main__':
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    main_frame = AboutBox(None)
    app.SetTopWindow(main_frame)
    main_frame.Show()
    app.MainLoop()

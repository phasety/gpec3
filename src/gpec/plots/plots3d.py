# -*- coding: utf-8 -*-
import numpy as np
from base import Plot3D
from wx.lib.pubsub import pub


class PTrho(Plot3D):
    def __init__(self, parent, arrays=None, system=(), **kwargs):

        self.short_title = u"P-T-\u03c1"
        self.title = (u'Pressure-Temperature-Density projection of an '
                      u'univariant phase equilibrium diagram')
        self.xlabel = u'Temperature [K]'
        self.ylabel = u'Density [mol/l]'
        self.zlabel = u'Pressure [bar]'
        Plot3D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel,
                        system, projection='3d', zlabel=self.zlabel, **kwargs)

    def setup_curves(self, arrays, **kwarg):
        if 'VAP' in arrays.keys():
            try:
                lines = []
                name = u'Pure compound vapor pressure lines'
                for num, vap_curve in enumerate(arrays['VAP']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(vap_curve.T[0], vap_curve.T[2],
                                            vap_curve.T[1], 'g', label=label),
                    lines += self.axes.plot(vap_curve.T[0], vap_curve.T[3],
                                            vap_curve.T[1], 'g', label='_nolegend_'),

                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'VAP',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 1'))

        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    if len(cri_curve) < 3:
                        continue
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(cri_curve.T[0], cri_curve.T[2],
                                            cri_curve.T[1], 'k', label=label)
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'black',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'CRI'
                                    })
            except Exception:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 2'))

        if 'LLV' in arrays.keys():
            lines = []
            name = 'LLV'
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label_b = 'Liquid in LLV' if num == 0 else '_nolegend_'
                    label_r = 'Vapor in LLV' if num == 0 else '_nolegend_'
                    lines += self.axes.plot(llv_curve.T[0], llv_curve.T[7],
                                            llv_curve.T[1], 'b', label=label_b)
                    lines += self.axes.plot(llv_curve.T[0], llv_curve.T[8],
                                            llv_curve.T[1], 'b', label='_nolegend_')
                    lines += self.axes.plot(llv_curve.T[0], llv_curve.T[9],
                                            llv_curve.T[1], 'r', label=label_r)
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'red',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'LLV',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 3'))
        if 'AZE' in arrays.keys():
            lines = []
            name = u'Azeotropic lines'
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(aze_curve.T[0], aze_curve.T[4],
                                            aze_curve.T[1], 'm', label=label)
                    lines += self.axes.plot(aze_curve.T[0], aze_curve.T[5],
                                            aze_curve.T[1], 'm', label='_nolegend_')
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'magenta',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'LLV',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 3'))

        if 'ISO' in arrays.keys():
            name = u'Isopleth lines (Z = %s)' % kwarg['z_val']
            lines = []
            try:
                for num, iso_curve in enumerate(arrays['ISO']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(iso_curve.T[0], iso_curve.T[4],
                                            iso_curve.T[1], 'g--', label=label),
                    lines += self.axes.plot(iso_curve.T[0], iso_curve.T[5],
                                            iso_curve.T[1], 'g--', label='_nolegend_'),
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'violet',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 4'))

        if 'Txy' in arrays.keys():
            name = u'Isobaric lines (P = %s)' % kwarg['p_val']
            lines = []
            try:
                for num, iso_curve in enumerate(arrays['Txy']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(iso_curve.T[0], iso_curve.T[5],
                                            float(kwarg['p_val']), 'k--', label=label),
                    lines += self.axes.plot(iso_curve.T[0], iso_curve.T[6],
                                            float(kwarg['p_val']), 'k--',
                                            label='_nolegend_'),
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 5'))

        if 'Pxy' in arrays.keys():
            name = u'Isothermal lines (T = %s)' % kwarg['t_val']
            lines = []
            try:
                for num, iso_curve in enumerate(arrays['Pxy']):
                    label = name if num == 0 else '_nolegend_'
                    t_constant = np.repeat(float(kwarg['t_val']), len(iso_curve.T[0]))
                    lines += self.axes.plot(t_constant, iso_curve.T[5], iso_curve.T[0],
                                            'k-.', label=label),
                    lines += self.axes.plot(t_constant, iso_curve.T[6], iso_curve.T[0],
                                            'k-.', label='_nolegend_')
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 6'))


class PTx(Plot3D):
    """PTx 3D projection"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"P-T-x"
        self.title = (u'Pressure-Temperature-Composition projection of'
                      u'a univariant phase equilibrium diagram')
        self.xlabel = u'Temperature [K]'
        self.ylabel = u'Composition' if not system else "%s molar fraction" % system[0]
        self.zlabel = u'Pressure [bar]'

        Plot3D.__init__(self, parent, arrays, self.title, self.xlabel, self.ylabel,
                        system, projection='3d', zlabel=self.zlabel, **kwargs)

    def setup_curves(self, arrays, **kwarg):
        if 'VAP' in arrays.keys():
            lines = []
            name = u'Pure compound vapor pressure'
            try:
                for num, vap_curve in enumerate(arrays['VAP']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(vap_curve.T[0],
                                            np.repeat(1 - num, len(vap_curve.T[0])),
                                            vap_curve.T[1], 'g', label=label),
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'VAP',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 7'))

        if 'CRI' in arrays.keys():
            lines = []
            name = u'Critical lines'
            try:
                for num, cri_curve in enumerate(arrays['CRI']):
                    if len(cri_curve) < 3:
                        continue
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(cri_curve.T[0], cri_curve.T[3],
                                            cri_curve.T[1], 'k', label=label)

                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'black',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'CRI'
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 8'))
        if 'LLV' in arrays.keys():
            lines = []
            name = 'LLV'
            try:
                for num, llv_curve in enumerate(arrays['LLV']):
                    label_b = 'Liquid in LLV' if num == 0 else '_nolegend_'
                    label_r = 'Vapor in LLV' if num == 0 else '_nolegend_'
                    lines += self.axes.plot(llv_curve.T[0], llv_curve.T[2],
                                            llv_curve.T[1], 'b', label=label_b)
                    lines += self.axes.plot(llv_curve.T[0], llv_curve.T[3],
                                            llv_curve.T[1], 'b', label='_nolegend_')
                    lines += self.axes.plot(llv_curve.T[0], llv_curve.T[4],
                                            llv_curve.T[1], 'r', label=label_r)
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'red',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'LLV',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 8'))

        if 'AZE' in arrays.keys():
            name = u'Azeotropic lines'
            lines = []
            try:
                for num, aze_curve in enumerate(arrays['AZE']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(aze_curve.T[0], aze_curve.T[2],
                                            aze_curve.T[1], 'm', label=label)
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'magenta',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'AZE',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 9'))

        if 'ISO' in arrays.keys():
            name = u'Isopleth saturated line  (Z = %s)' % kwarg['z_val']
            lines = []
            try:
                for num, iso_curve in enumerate(arrays['ISO']):
                    label = name if num == 0 else '_nolegend_'
                    max_val = locals().get('max_val', np.max(iso_curve.T[2]))
                    saturated = np.repeat(max_val, len(iso_curve.T[1]))
                    lines += self.axes.plot(iso_curve.T[0], saturated, iso_curve.T[1],
                                            'g--', label=label),
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'violet',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 10'))

        if 'Txy' in arrays.keys():
            name = u'Isobaric lines (P = %s)' % kwarg['p_val']
            lines = []
            try:
                for num, iso_curve in enumerate(arrays['Txy']):
                    label = name if num == 0 else '_nolegend_'
                    lines += self.axes.plot(iso_curve.T[0], iso_curve.T[1],
                                            float(kwarg['p_val']), 'k--', label=label),
                    lines += self.axes.plot(iso_curve.T[0], iso_curve.T[2],
                                            float(kwarg['p_val']), 'k--',
                                            label='_nolegend_'),
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 11'))

        if 'Pxy' in arrays.keys():
            name = u'Isothermal lines (T = %s)' % kwarg['t_val']
            lines = []
            try:
                for num, iso_curve in enumerate(arrays['Pxy']):
                    label = name if num == 0 else '_nolegend_'
                    t_constant = np.repeat(float(kwarg['t_val']), len(iso_curve.T[0]))
                    lines += self.axes.plot(t_constant, iso_curve.T[1], iso_curve.T[0],
                                            'k-.', label=label),
                    lines += self.axes.plot(t_constant, iso_curve.T[2], iso_curve.T[0],
                                            'k-.', label='_nolegend_')
                self.curves.append({'name': name,
                                    'visible': True,
                                    'lines2d': lines,
                                    'color': 'green',
                                    'wx_id': self.get_unique_GUI_id(),
                                    'type': 'ISO',
                                    })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in GPECOUT.DAT 12'))

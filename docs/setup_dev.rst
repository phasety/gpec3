Setup a GPEC development enviroment on Linux
============================================

:author: Martín Gaitán <gaitan@phasety.com>
:date: sept 26, 2012


Introduction
-------------

This document is a guide to install an enviroment 
to contribute to the development of GPEC. It's fully based on modern 
Ubuntu Linux distributions (particularly Ubuntu 12.04) but should be 
straight forward follow this on any other linux distribution. 


Install system packages
------------------------

1. Install things::

      $ sudo apt-get install git python-dev python-pip wine gfortran \
						    python-wxversion python-wxgtk2.8 g++ \
							libfreetype6-dev libpng12-dev xclip 
      $ sudo pip install virtualenvwrapper 


.. note:: 

	To code, you'll also need an editor. Could be any you prefer!
	Some people prefer simple ones like ``gedit`` (installed by default) 
	or geany. Some other prefers something more geeky like ``vim`` 
	or even more complete tools like ``pydev`` or ``ninja-ide``. 


Make a virtual enviroment
-------------------------

We use virtualenv_ (using virtualenvwrapper_) to 
isolate our enviroment for other projects and system-wide python packages

2. Setup virtualenvwrapper

   2.1  Create a directory to hold your virtualenvs::

     mkdir ~/.virtualenvs
     mkdir ~/projects
     mkdir ~/.pip_download_cache

   2.2  Edit you ``~/.bashrc`` adding this lines::

     export WORKON_HOME=$HOME/.virtualenvs
     source /usr/local/bin/virtualenvwrapper.sh
     export GIT_SSL_NO_VERIFY=true           #not virtualenv related but needed
     export PROJECT_HOME=$HOME/projects      #folder for new projects. Could be what you want
     export PIP_DOWNLOAD_CACHE=$HOME/.pip_download_cache

  2.3  And reload that::

    $ source ~/.bashrc

3. Now create the project gpec:: 

    $ mkproject gpec

  This create new virtualenv in the WORKON_HOME binded to a 
  project directory in PROJECT_HOME

.. note::

    Next times, when you want to active the gpec's virtualenv you'll run::

        $ workon gpec

    When you want to deactive the virtualenv, on any path ::

        (gpec)~/projects/gpec$ deactivate

Checkout the code
-----------------

Gpec's git repo isn't public, so you need to have credentials to read 
and/or write it. Please if you don't have a Bitbucket account, create one:

1. Go to https://bitbucket.org/account/signup/ and sign up

2. Let me know (gaitan@phasety.com) your username 
   and ask for dev permissions on Gpec. If you don't have an user
   on bitbucket also let me know first. I'll send and invitation

3. Setup a ssh-key_ ::

	$ ssh-keygen
	$ xclip -sel clip < ~/.ssh/id_rsa.pub
	
	and paste this on 
	https://bitbucket.org/account/user/<your_userr>/ssh-keys/

4. Then we go::

	(gpec)~/projects/gpec$ git clone git@bitbucket.org:phasety/gpec3.git .
	(gpec)~/projects/gpec$ git checkout develop

5. Remember configure your identity (so, your future great code will 
   be recognized)::

	(gpec)$ git config --global user.name "Juan Perez"
    (gpec)$ git config --global user.email perez@phasety.com


Almost done
------------

- Bypass wxpython and wxversion to the ones installed via apt-get::

	(gpec)$ add2virtualenv /usr/lib/python2.7/dist-packages/wx-2.8-gtk2-unicode
    (gpec)$ cdsitepackages
    (gpec)$ ln -s /usr/lib/python2.7/dist-packages/wxversion.py .

.. tip:: 

	Ensure this worked. You should get some numbers as output ::
	
		(gpec) $ python -c 'import wxversion, wx; print wx.__version__'
		 2.8.12.1

- Install gpec's requirements (basically numpy_ and matplotlib_) via pip::

	(gpec)~/projects/gpec$ pip install -r setup/pip_requirement.txt

- And finally install GPEC 

	(gpec)~/projects/gpec$ pip install -e ./src

.. tip:: 

	The flag ``-e`` on the last command means this package will be *editable*. 
	Every change on the code of gpec will impact automatically 
	

Last test
-------------

You should have an executable called *gpec*:: 

	(gpec) $ gpec

And also a package called *gpec*::

	(gpec) $ ipython

	In [1]: import gpec
	
	In [2]: gpec.__version__
	Out[2]: '3.1.1'	

How to know more
--------------------

There is no dev documentation further than this, but the report
of Martin Gaitan's Final project is still useful as a general overview
Download it from here:

    http://code.google.com/p/gpec2010/downloads/detail?name=report.pdf
    
and **¡Happy coding!**









.. _virtualenv: http://www.virtualenv.org
.. _virtualenvwrapper: http://www.doughellmann.com/projects/virtualenvwrapper/
.. _ssh-key: https://confluence.atlassian.com/pages/viewpage.action?pageId=270827678
.. _numpy: http://numpy.org/
.. _matplotlib: http://matplotlib.org/




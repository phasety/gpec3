GPEC is a software to obtain phase diagrams and other thermodynamic plots 
for binary mixtures, as calculated with equations of state. It can be 
helpful for either educational, academic or development purposes. 
It is easy to use and you do not have to provide any initial guesses or 
other inputs.



#!/usr/bin/env python
# -*- coding: utf-8 -*-
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas
from mpl_toolkits.mplot3d.axes3d import Axes3D

import wx
from wx.lib.pubsub import pub

from  gpec.ui.dialogs import ZoomDialog


class BasePlot(object):
    """a base plot class for GPEC"""

    def __init__(self, parent, arrays=None, title=None, xlabel="", ylabel="",
                 system=(), projection="2d", zlabel="", **karg):

        self.title = title
        self.system = system[:]
        self.arrays = arrays
        self.parent = parent

        self.fig = Figure()     # dpi=self.dpi

        self.canvas = FigCanvas(parent, -1, self.fig)
        self.projection = projection

        if self.system:

            ntdep, kij = self.system[3]
            if ntdep == 1:
                kij = "%s + %s*e^(-T/%s)" % tuple(map(str, kij))
            self.system[3] = kij
            case_label = "%s + %s, %s EOS, Kij = %s, Lij = %s" % tuple(self.system)
            self.fig.text(0.01, 0.01, case_label, fontsize=9,
                          horizontalalignment='left')

        self.properties_check = {'Grid': self.get_unique_GUI_id(),
                                 'Legends': self.get_unique_GUI_id()}

        self.properties_normal = {'Set plot title...':
                                  self.get_unique_GUI_id()}

        if projection == '3d':
            self.properties_normal['Set perspective...'] = self.get_unique_GUI_id()
        else:
            self.properties_normal['Zoom to...'] = self.get_unique_GUI_id()

        self.curves = []    # curves to plot

    def setup_curves(self):
        """each one subtype declare curves as a group of lines2d
            - redefined by each sub class"""
        pass

    def plot(self):
        """plot all visible curves"""
        for curve in self.curves:
            if curve['visible'] and 'lines' in curve:
                if not '_is_a_point' in curve or not curve['_is_a_point']:
                    marker = '' if 'marker' not in curve.keys() else curve['marker']
                    curve['lines2d'] = self.axes.plot(*curve['lines'],
                                                      color=curve['color'],
                                                      marker=marker,
                                                      label=curve['name'])

                else:
                    x = [curve['lines'][0]]
                    y = [curve['lines'][1]]
                    marker = '' if 'marker' not in curve.keys() else curve['marker']
                    curve['lines2d'] = self.axes.scatter(x, y, color=curve['color'],
                                                         marker=marker,
                                                         label=curve['name'])
        self.legend()
        self.canvas.draw()

    def legend(self):
        if self.axes.get_legend():
            self.axes.legend_ = None  # a tricky way to remove lengeds
        else:
            self.axes.legend(loc='best', scatterpoints=1)

    def OnToggleCurve(self, event):
        wx_id = event.GetId()
        curve = [curve for curve in self.curves if curve['wx_id'] == wx_id][0]
        #TODO better way?
        curve['visible'] = not curve['visible']
        self.refresh_curve(curve)

    def refresh_curve(self, curve, add_lines=False):
        for line in curve['lines2d']:
            if add_lines:
                self.axes.add_line(line)
            try:
                line.set_visible(curve['visible'])
            except AttributeError:
                for line_ in line:
                    line_.set_visible(curve['visible'])
        self.canvas.draw()

    def OnSetupProperty(self, event):
        wx_id = event.GetId()
        prop = dict([[v, k] for k, v in
                    self.properties_normal.items()])[wx_id]     # inverted dict
        if prop == 'Set perspective...':
            dlg = wx.TextEntryDialog(self.parent,
                                     u'Azimuthal angle',
                                     u'Set Perpective',
                                     defaultValue=str(self.axes.azim))
            r = dlg.ShowModal()
            if  r == wx.ID_CANCEL:
                return
            elif r == wx.ID_OK:
                azim = float(dlg.GetValue())
            dlg = wx.TextEntryDialog(self.parent, u'Elevation angle',
                                     u'Set Perpective', defaultValue=str(self.axes.elev))
            r = dlg.ShowModal()
            if r == wx.ID_CANCEL:
                return
            elif r == wx.ID_OK:
                elev = float(dlg.GetValue())
            self.axes.view_init(elev, azim)
        elif prop == 'Set plot title...':
            dlg = wx.TextEntryDialog(self.parent, u'Plot title', u'Set plot title',
                                     defaultValue=self.axes.get_title())
            r = dlg.ShowModal()
            if  r == wx.ID_CANCEL:
                return

            self.title = dlg.GetValue()
            self.axes.set_title(self.title)

        elif prop == 'Zoom to...':
            values = self.axes.axis()
            dlg = ZoomDialog(self.parent, 'Zoom to', data=values)
            r = dlg.ShowModal()
            if  r == wx.ID_OK:
                values = dlg.GetValues()
                self.axes.axis(values)
        self.canvas.draw()

    def OnToggleProperty(self, event):
        wx_id = event.GetId()
        prop = dict([[v, k] for k, v in
                    self.properties_check.items()])[wx_id]  # inverted dict
        if prop == 'Grid':
            self.axes.grid()

        elif prop == 'Log X':
            if self.axes.get_xscale() == 'linear':
                self.axes.set_xscale('log')
            else:
                self.axes.set_xscale('linear')
        elif prop == 'Log Y':
            if self.axes.get_yscale() == 'linear':
                self.axes.set_yscale('log')
            else:
                self.axes.set_yscale('linear')
        elif prop == 'Legends':
            if self.axes.get_legend():
                self.axes.legend_ = None    # a tricky way to remove lengeds
            else:
                self.axes.legend(loc='best')
        self.canvas.draw()

    def get_unique_GUI_id(self):
        return wx.NewId()

    def sendMessage(self, topic, message):
        """abstraction of pubsub. every message in plot is sent to this method
            as a enter point. This convert plots agnostics of pubsub"""
        try:
            pub.sendMessage(topic, message)
        except NameError:
            #no pubsub. no message
            pass


class Plot2D(BasePlot):

    def __init__(self, parent, arrays=None, title=None, xlabel="",
                 ylabel="", system=(), projection="2d", zlabel="", **kwargs):
        BasePlot.__init__(self, parent, None, title, xlabel, ylabel, system,
                          projection=projection, zlabel=zlabel, **kwargs)
        self.properties_check['Log X'] = self.get_unique_GUI_id()
        self.properties_check['Log Y'] = self.get_unique_GUI_id()
        self.axes = self.fig.add_subplot(111)
        self.axes.set_title(title)
        self.axes.set_ylabel(ylabel)
        self.axes.set_xlabel(xlabel)
        self.axes.set_autoscale_on(True)
        if arrays:
            self.setup_curves(arrays)
            self.plot()


class Plot3D(BasePlot):
    def __init__(self, parent, arrays=None, title=None, xlabel="", ylabel="",
                 system=(), projection="3d", zlabel="", **kwargs):
        BasePlot.__init__(self, parent, None, title, xlabel, ylabel, system,
                          projection=projection, zlabel=zlabel, **kwargs)
        self.axes = Axes3D(self.fig)
        self.axes.set_ylabel(ylabel, fontdict={'fontsize': 14})
        self.axes.set_xlabel(xlabel, fontdict={'fontsize': 14})
        self.axes.set_zlabel(zlabel, fontdict={'fontsize': 14})
        self.fig.suptitle(title, fontdict={'fontsize': 15})
        if arrays:
            self.setup_curves(arrays)
            self.plot()

# -*- coding: utf-8 -*-
import os
import sys
import time
import glob

import wx
import wx.lib.agw.aui as aui
from wx.lib.pubsub import pub
from gpec.settings import PATH_ICONS, IPYTHON_CONSOLE
from gpec.tools.misc import open_folder


class InfoPanel(wx.Panel):
    """a general tabbed panel including a log list and other useful information"""

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        self.nb = aui.AuiNotebook(self, style=aui.AUI_NB_TOP | aui.AUI_NB_TAB_SPLIT )

        #by default it include a log_messages panel
        self.log_panel = LogMessagesPanel(self, -1)
        self.io_panel = IOPanel(self, -1)
        self.shell_panel = ShellPanel(self, -1)
        self.nb.AddPage(self.log_panel, "Log")
        self.nb.AddPage(self.io_panel, "Input/Output ")
        self.nb.AddPage(self.shell_panel, "Shell")
        sizer = wx.BoxSizer()
        sizer.Add(self.nb, 1, wx.EXPAND)
        self.SetSizerAndFit(sizer)

class IOPanel(wx.Panel):
    """an info panel to show input an output text files"""

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        #TREE
        self.tree = wx.TreeCtrl(self, -1, wx.DefaultPosition, wx.DefaultSize,
                               wx.TR_DEFAULT_STYLE | wx.TR_HIDE_ROOT)
        self.root = self.tree.AddRoot("")
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnTreeSelChanged, self.tree)
        self.Bind(wx.EVT_CONTEXT_MENU, self.OnContextMenu, self.tree)

        #TEXT
        self.text_ctrl = wx.TextCtrl(self, -1,  style=wx.TE_MULTILINE|wx.TE_READONLY)
        #TODO make this through a sash

        #sizer
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(self.tree, 1, wx.EXPAND)
        sizer.Add(self.text_ctrl, 4, wx.EXPAND)
        self.SetSizerAndFit(sizer)
        self.cases = {} #id:item_ID
        pub.subscribe(self.OnAddItem, 'add_txt')

    def OnContextMenu(self, event):
        if not hasattr(self, "open_folder"):
            self.open_folder = wx.NewId()
            self.Bind(wx.EVT_MENU, self.OnPopupOpenFolder, id=self.open_folder)
        # make a menu
        menu = wx.Menu()
        # Show how to put an icon in the menu
        item = wx.MenuItem(menu, self.open_folder, "Open I/O folder")
        menu.AppendItem(item)
        self.PopupMenu(menu)
        menu.Destroy()

    def OnPopupOpenFolder(self, event):
        # open folder for the current selected tree item
        item = self.tree.GetSelection()
        (head, content) = self.tree.GetItemPyData(item)
        open_folder(head)

    def OnTreeSelChanged(self, event):
        item = event.GetItem()
        try:
            (head, content) = self.tree.GetItemPyData(item)
            self.text_ctrl.Clear()
            self.text_ctrl.AppendText(content)
        except:
            pass

    def OnAddItem(self, msg):

        filepath, case_id = msg.data

        head,filename = os.path.split(filepath)

        if case_id not in self.cases.keys():
            #if case is unknown, create it
            node = self.tree.AppendItem(self.root, "Case %d (%s)" % (case_id, head))
            self.cases[case_id] = node
        else:
            node = self.cases[case_id]


        with open( filepath, 'r') as fh:
            content = fh.read()
            item = self.tree.AppendItem(node, filename)
            self.tree.SetPyData(item, (head, content))


        #self.tree.Expand(node)
        self.tree.SelectItem(item)  #TODO refresh text_ctrl?



class LogMessagesPanel(wx.Panel):
    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        self.list = wx.ListCtrl(self, -1,  style=  wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.setupList()
        sizer = wx.BoxSizer()
        sizer.Add(self.list, 1, wx.EXPAND)
        self.SetSizerAndFit(sizer)
        pub.subscribe(self.OnAppendLog, 'log')

    def setupList(self):
        """sets columns and append a imagelist """
         #setup first column (which accept icons)
        info = wx.ListItem()
        info.m_mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_IMAGE | wx.LIST_MASK_FORMAT
        info.m_image = -1
        info.m_format = 0
        info.m_text = "Message"
        self.list.InsertColumnInfo(0, info)
        self.list.SetColumnWidth(0, 550)

        #insert second column
        self.list.InsertColumn(1, 'Time')
        self.list.SetColumnWidth(1, 70)

        #setup imagelist and an associated dict to map status->image_index
        imgList = wx.ImageList(16, 16)
        ico_dir = os.path.join(PATH_ICONS, 'log')
        ico_list = glob.glob(os.path.join(ico_dir, '*.png'))

        self.icon_map = {}
        for ico_path in ico_list:
            indx = imgList.Add( wx.Bitmap(ico_path, wx.BITMAP_TYPE_PNG))
            ico_name = os.path.basename(ico_path)[:-4]
            self.icon_map[ico_name] = indx
        self.list.AssignImageList(imgList, wx.IMAGE_LIST_SMALL)

    def OnAppendLog(self, msg):
        ico = self.icon_map[msg.data[0]]
        message = msg.data[1]
        index = self.list.InsertImageStringItem(sys.maxint, message, ico)
        self.list.SetStringItem(index, 1, time.strftime('%H:%M:%S'))
        self.list.EnsureVisible(index) #keep scroll at bottom
        if msg.data[0] == 'error':
            wx.Bell()

class ShellPanel(wx.Panel):
    """A python shell """

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)

        intro='Welcome To GPEC\n'

        if IPYTHON_CONSOLE:
            try:
                import IPython.gui.wx.wxIPython as wxIP
                self.shell = wxIP.IPShellWidget(self, intro)
            except ImportError:
                try:
                    import  wx.py  as  py #for pyshell
                    self.shell = py.shell.Shell(self, -1, introText=intro )
                except :
                    self.shell = wx.Panel(self, -1)
        else:
            self.shell = wx.Panel(self, -1)

        sizer = wx.BoxSizer()
        sizer.Add(self.shell, 1, wx.EXPAND)
        self.SetSizerAndFit(sizer)

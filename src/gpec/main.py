#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import sys
import wx
from wx.lib.pubsub import setuparg1   # NOQA
from wx.lib.pubsub import pub
from gpec.aui import MainFrame


def main():
    all_plots = {}

    def add_plot_instance(message):
        all_plots[message.data[0]] = message.data[1]

    def remove_plot_instance(message):
        if message.data in all_plots:
            all_plots.pop(message.data)

    #pub.subscribe(add_plot_instance, 'add_plot_instance')
    #pub.subscribe(remove_plot_instance, 'remove_plot_instance')

    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    main_frame = MainFrame(None, -1)
    app.SetTopWindow(main_frame)
    if len(sys.argv) > 1:
        main_frame._open_file(sys.argv[1])
    else:
        pub.sendMessage('log', ('ok', 'GPEC is ready. Define a system to begin'))
    main_frame.Show()

    app.MainLoop()


if __name__ == '__main__':
    main()

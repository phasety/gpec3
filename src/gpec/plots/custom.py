# -*- coding: utf-8 -*-

from base import Plot2D

class CustomPlot(Plot2D):
    def __init__ (self, parent, title, xlabel, ylabel, projection="2d", zlabel="", system=(), **kwargs):
        Plot2D.__init__(self, parent, None, title, xlabel, ylabel, system, projection=projection, zlabel=zlabel, **kwargs)


    def setup_curves(self, *curves):
        #copy all curves from source plots
        self.curves = [c for curve in curves for c in curve]

        #replot lines data from each curve
        for curve in self.curves:
            new_lines = []
            for line in curve['lines2d']:
                kwargs = {'color': line.get_color(),
                          'linestyle': line.get_linestyle(),
                          'marker':line.get_marker(),
                          'label': line.get_label(),
                          }
                if self.projection == '3d':
                    x, y, z = line._verts3d  #why the hell there is no get_zdata()
                    new_lines += self.axes.plot(x, y, z, **kwargs) 
                else:
                    x, y = line.get_data()
                    new_lines += self.axes.plot(x, y, **kwargs)

            #update lines set on curve dict (used in toggle curve, for example)
            curve['lines2d'] = new_lines
            

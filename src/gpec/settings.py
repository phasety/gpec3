#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import os
import sys
import tempfile

work_in_memory = False       # if temporary files are written to memory.

home = os.path.dirname(os.path.abspath(sys.argv[0]))
data_dir = os.path.join(home, "data")
if not os.path.isdir(data_dir):
    home2 = os.path.join(os.path.dirname(__file__), os.path.pardir)
    data_dir = os.path.join(home2, "data")

#path where fortran executables are

PATH_BIN = os.path.join(data_dir, "backend")
PATH_ICONS = os.path.join(data_dir, "icons")
PATH_DB = os.path.join(data_dir, "db", "gpec.sqlite")
PATH_TEMP = tempfile.mkdtemp(prefix="gpec_")


class Model():
    SRK = 1
    PR = 2
    RKPR = 3
    PCSAFT = 4
    SPHCT = 6


EOS = {'Soave-Redlich-Kwong': Model.SRK,
       'Peng-Robinson': Model.PR,
       'RK-PR': Model.RKPR,
       'PC-SAFT': Model.PCSAFT,
       'SPHCT': Model.SPHCT}

INV_EOS = dict([[v, k] for k, v in EOS.items()])

EOS_SHORT = {Model.SRK: 'SRK',
             Model.PR: 'Peng-Robinson',
             Model.RKPR: 'RK-PR',
             Model.PCSAFT: 'PC-SAFT',
             Model.SPHCT: 'SPHCT'}

VC_RATIO_DEFAULT = 1.168   # relation MODEL/EXPERIMENTAL for critical volumen
TIMEOUT = 10    # seconds to timeout the calculation

IPYTHON_CONSOLE = False

WEIGHT_POWER = 14  # weight = (Tc ^ WEIGHT_POWER) / Pc

BIN_AVAILABLE = {'2PhPxy': {'in': ['twophin.DAT2'], 'out': ['PXYOUT.DAT']},
                 '2PhTxy': {'in': ['twophin.DAT2'], 'out': ['TXYOUT.DAT']},
                 'FUGi': {'in': ['GPECIN.DAT', 'FUGIN.DAT'], 'out': ['FUGOUT.DAT']},
                 'GPEC': {'in': ['GPECIN.DAT'], 'out': ['GPECOUT.DAT']},
                 'IsoplethGPEC': {'in': ['GPECIN.DAT', 'GPECOUT.DAT', 'ZforIsop.dat'],
                                  'out': ['ISOPOUT.DAT']},
                 'IsoXT': {'in': ['GPECIN.DAT', 'IsoXTin.DAT'],
                           'out': ['IsoXTout.DAT']},
                 'Models': {'in': [], 'out': []},
                 'ModelsParam': {'in': ['CONPARIN.DAT'], 'out': ['CONPAROUT.DAT']},
                 'PCSAFT': {'in': ['CONPARIN.DAT'], 'out': ['CONPAROUT.DAT']},
                 'RKPRParam': {'in': ['CONPARIN.DAT'], 'out': ['CONPAROUT.DAT']},
                 'PxyGPEC': {'in': ['GPECIN.DAT', 'GPECOUT.DAT', 'TFORPXY.dat'],
                             'out': ['PXYOUT.DAT']},
                 'TxyGPEC': {'in': ['GPECIN.DAT', 'GPECOUT.DAT', 'PFORTXY.dat'],
                             'out': ['TXYOUT.DAT']}
                 }


COMBINING_RULES = {0: 'van Der Waals', 1: 'Lorentz-Berthelot'}

PLOT_SUITES = {'globalsuite': ['PT', 'Tx', 'Px', 'Trho', 'Prho'],
               'isop': ['IsoPT', 'IsoTx', 'IsoPx', 'IsoTrho', 'IsoPrho'],
               'pxy': ['Pxy', 'PxyPrho'],
               'txy': ['Txy', 'TxyTrho']
               }

PLOT_IN_3D = True   # just to disallow 3d for speedup on testing purpose

RKPR_MSG = "You can choose what to specify,\nbesides Tc, Pc and the acentric factor"
KIJ_MSG = u"Kij may be constant or \ntemperature dependent"

STATS_URL = 'http://stats.phasety.com'

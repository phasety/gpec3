# -*- coding: utf-8 -*-

from base import Plot2D
from wx.lib.pubsub import pub


class Txy(Plot2D):
    """Txy Tx (isobaric)"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"Txy (isobaric)"
        self.title = u'Isobaric fluid phase equilibrium'
        if 'p' in kwargs:
            self.title += ' for P=%s [bar]' % kwargs['p']
        self.ylabel = u'Temperature [k]'
        # TO DO: DEFINE system inside the plot
        self.xlabel = (u"Composition" if not system
                       else "%s molar fraction" % system[0])
        Plot2D.__init__(self, parent, arrays, self.title,
                        self.xlabel, self.ylabel, system, **kwargs)

    def legend(self):
        pass

    def setup_curves(self, arrays):
        if 'Txy' in arrays.keys():
            try:
                for num, isob_curve in enumerate(arrays['Txy']):
                    counter = (u'' if len(arrays['Txy']) == 1
                               else u' %i' % (num + 1))
                    self.curves.append({'name': u'Isobaric lines' + counter,
                                        'visible': True,
                                        'lines': (isob_curve.T[1], isob_curve.T[0],
                                                  isob_curve.T[2], isob_curve.T[0]),
                                        'color': 'black',
                                        'type': 'Txy'
                                        })
            except Exception:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in TXYOUT.DAT'))


class TxyTrho(Plot2D):
    """Txy T-Rho projection (isobaric)"""
    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"T-\u03c1 (isobaric)"
        self.title = u'Temperature-Density of the Isobaric fluid phase equilibrium'
        if 'p' in kwargs:
            self.title += ' for P=%s [bar]' % kwargs['p']
        self.ylabel = u'Temperature [k]'
        self.xlabel = u'Density [mol/l]'    # TO DO: DEFINE system inside the plot

        Plot2D.__init__(self, parent, arrays, self.title,
                        self.xlabel, self.ylabel, system, **kwargs)

    def legend(self):
        pass

    def setup_curves(self, arrays):
        if 'Txy' in arrays.keys():
            try:
                for num, isob_curve in enumerate(arrays['Txy']):
                    counter = u'' if len(arrays['Txy']) == 1 else u' %i' % (num + 1)

                    self.curves.append({'name': u'Isobaric lines' + counter,
                                        'visible': True,
                                        'lines': (isob_curve.T[5], isob_curve.T[0],
                                                  isob_curve.T[6], isob_curve.T[0]),
                                        'color': 'black',
                                        'wx_id': self.get_unique_GUI_id(),
                                        'type': 'Pxy'
                                        })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in TXYOUT.DAT'))

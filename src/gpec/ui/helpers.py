import os
import pickle
import gpec.apimanager
from gpec.tools.misc import Counter


def open_file(path):
    """given a path, return its basename and data"""

    Counter().reset()      # reset the singleton
    gpec.apimanager.clean_tmp()

    with open(path, 'rb') as fh:
        data = pickle.load(fh)

    return os.path.basename(path), data


    # -*- coding: utf-8 -*-

from base import Plot2D
from wx.lib.pubsub import pub


class Pxy(Plot2D):
    """Pxy Px (isothermal)"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"Pxy (isothermal)"
        self.title = u'Isothermal fluid phase equilibrium'
        if 't' in kwargs:
            self.title += ' for T=%s [k]' % kwargs['t']
        self.ylabel = u'Pressure [bar]'
        # TO DO DEFINE system inside the plot
        self.xlabel = (u'Composition' if not system
                       else "%s molar fraction" % system[0])
        Plot2D.__init__(self, parent, arrays, self.title,
                        self.xlabel, self.ylabel, system, **kwargs)

    def legend(self):
        pass

    def setup_curves(self, arrays):
        if 'Pxy' in arrays.keys():
            try:
                for num, isot_curve in enumerate(arrays['Pxy']):
                    counter = u'' if len(arrays['Pxy']) == 1 else u' %i' % (num + 1)
                    self.curves.append({'name': u'Isothermal lines ' + counter,
                                        'visible': True,
                                        'lines': (isot_curve.T[1], isot_curve.T[0],
                                                  isot_curve.T[2], isot_curve.T[0]),
                                        'color': 'black',
                                        'wx_id': self.get_unique_GUI_id(),
                                        'type': 'Pxy'
                                        })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in PXYOUT.DAT'))


class PxyPrho(Plot2D):
    """Pxy P-Rho projection (isothermal)"""

    def __init__(self, parent, arrays=None, system=(), **kwargs):
        self.short_title = u"P-\u03c1 (isothermal)"
        self.title = u'Pressure-Density of the Isothermal fluid phase equilibrium'
        if 't' in kwargs:
            self.title += ' for T=%s [k]' % kwargs['t']

        self.ylabel = u'Pressure [bar]'
        # TO DO DEFINE system inside the plot
        self.xlabel = u'Density [mol/l]'
        Plot2D.__init__(self, parent, arrays, self.title,
                        self.xlabel, self.ylabel, system, **kwargs)

    def legend(self):
        pass

    def setup_curves(self, arrays):
        if 'Pxy' in arrays.keys():
            try:
                for num, isot_curve in enumerate(arrays['Pxy']):
                    counter = u'' if len(arrays['Pxy']) == 1 else u' %i' % (num + 1)
                    self.curves.append({'name': u'Isothermal lines ' + counter,
                                        'visible': True,
                                        'lines': (isot_curve.T[5], isot_curve.T[0],
                                                  isot_curve.T[6], isot_curve.T[0]),
                                        'color': 'black',
                                        'wx_id': self.get_unique_GUI_id(),
                                        'type': 'Pxy'
                                        })
            except:
                pub.sendMessage('error plot', None)
                pub.sendMessage('log', ('error', u'Invalid data in PXYOUT.DAT'))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import wx.aui
import os
import sys
import pickle

import about

from panels import SuitePlotsPanel, TabbedCases, InfoPanel, PlotsTreePanel

from wx.lib.pubsub import pub

from gpec.settings import PATH_ICONS
from ui.helpers import open_file
from gpec import __full_title__, __version__
from gpec.tools.misc import ping


class MainFrame(wx.Frame):

    def __init__(self, parent, id=-1,
                 pos=wx.DefaultPosition, title=__full_title__, size=(800, 600),
                 style=wx.DEFAULT_FRAME_STYLE | wx.MAXIMIZE):
        wx.Frame.__init__(self, parent, id, title, pos, size, style=style)

        favicon = wx.Icon(os.path.join(PATH_ICONS, "gpec48x48.png"),
                          wx.BITMAP_TYPE_PNG, 48, 48)
        wx.Frame.SetIcon(self, favicon)
        self._mgr = wx.aui.AuiManager(self)

        self.SetBackgroundColour(wx.NullColour)     # hack for win32

        self.title = title

        self.CenterOnScreen()

        self.CreateStatusBar()
        self.SetStatusText("")
        pub.subscribe(self.OnSetStatusText, 'status')

        menu = [
            ('&File', [
                ('&Open', self.FileOpen),
                ('&Save As...', self.FileSaveAs),
                ('&Save', self.FileSave),

                (),
                ('&Exit', self.onCloseWindow, "Quit this program"),

            ]),
#           Removed from menu until implementation

#            ('&Edit', [
#                ('&Copy', self.EditCopy),
#                ('&Paste', self.EditPaste),
#            ]),
            ('&View', [
                ('&Restore default view', self.SetDefaultPerspective),

                #('Sub&menu', [
                #    ('&Three', curry(self.DataBox, 3)),
                #    ('&Four', curry(self.DataBox, 4)),
                #]),
            ]),
            ('&Help', [
                ('&About', lambda evt: about.AboutBox(self)),

            ]),
        ]

        self.SetMenuBar(self.BuildMenu(menu))

        # create several text controls
        self.cases_panel = TabbedCases(self, -1)

        #ugly hack to fix the size of case aui panel
        if sys.platform != 'win32':
            the_size = self.cases_panel.GetSize()
        else:
            the_size = wx.Size(330, -1)

        self.cases_auipane = wx.aui.AuiPaneInfo().Name("cases").\
                          Caption(u"Cases").Left().MinSize(the_size).MaxSize(the_size).\
                          Layer(1).Position(2).CloseButton(True).MinimizeButton(True)

                          #MaxSize(self.cases_panel.GetSize()).\
        self.plots_panel = SuitePlotsPanel(self, -1)
        self.log_panel = InfoPanel(self, -1)


        self.plots_tree_panel = PlotsTreePanel(self, -1)
        self.plots_tree_auipane = wx.aui.AuiPaneInfo().\
                          Name("plots_tree").Caption("Manager").\
                          Floatable(True)

        self._mgr.AddPane(self.plots_tree_panel,self.plots_tree_auipane )

        # add the panes to the manager
        self._mgr.AddPane(self.cases_panel, self.cases_auipane )

        self._mgr.AddPane(self.plots_panel, wx.aui.AuiPaneInfo().Name('plot').Center().Layer(2).Caption(u"Plots").MaximizeButton(True))
        self._mgr.AddPane(self.log_panel, wx.aui.AuiPaneInfo().Name("log").Caption(u"Info").
                          MinSize(wx.Size(-1, 100)).
                          Bottom().Layer(0).Position(1).CloseButton(True).MaximizeButton(False))


        self.perspective_default = self._mgr.SavePerspective()

        # tell the manager to 'commit' all the changes just made
        self._mgr.Update()

        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.aui.EVT_AUI_PANE_CLOSE, self.OnClosePane)

        #self.Bind(wx.aui.EVT_AUI_RENDER, self.OnDragSash)

        self.Maximize()


        self.dirname = ''

        self.filename = None
        self.modified = False


        #a few hackies to refresh the ugly window
        pub.subscribe(self.RefreshMe, 'add checkbox')
        pub.subscribe(self.RefreshMe, 'log')
        pub.subscribe(self.RefreshMe, 'refresh all')

        self.registered = {}
        pub.subscribe(self.Register, 'register')


    def Register(self,  message):
        self.registered[message.data[0]] = message.data[1]

    def RefreshMe(self, message):
       self.Refresh()



    def OnDragSash(self, evt):
        print 'dragged', evt


    def FileSaveAs(self,event):
        """Save the project as a new file"""
        dlg = wx.FileDialog(self, "Save as", self.dirname, "", "*.gpc", \
                wx.SAVE | wx.OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            # Open the file for write, write, close
            self.filename=dlg.GetFilename()
            if self.filename[-4:] != '.gpc':
                self.filename += '.gpc'
            self.dirname=dlg.GetDirectory()
            self.FileSave(event)

        dlg.Destroy()

    def FileSave(self,event):
        """Save the project in the file defined or in a new one"""

        if self.filename is None:
            self.FileSaveAs(event)
        else:
            data = self.cases_panel.SaveCases()

            with open(os.path.join(self.dirname, self.filename),'wb') as fh:
                pickle.dump(data,fh)

            self.SetTitle("%s <%s>" % (self.title, self.filename))
            self.modified = False


    def OnClosePane(self, event):
        wx.CallAfter(self.egg)

    def egg(self):
        if all([not p.IsShown() for p in self._mgr.GetAllPanes()]):
            import grgevf   # NOQA

    def SetDefaultPerspective(self, event=None):
        self._mgr.LoadPerspective(self.perspective_default)


    def OnSetStatusText(self, message):
        self.SetStatusText(message.data)

    def OnClose(self, event):
        # ping stats and check for new versions
        response = ping()
        if (response and response.get('last_version', '') != __version__ and
                'release_date' in response and 'url_download' in response):

            msg = ("Gpec %s was released on %s.\n\nYou can download it from %s" %
                   (response['last_version'],
                    response['release_date'],
                    response['url_download'],
                    )
                   )
            wx.MessageBox(msg, 'New version available', wx.OK | wx.ICON_INFORMATION)

        # deinitialize the frame manager
        self._mgr.UnInit()
        # delete the frame
        self.Destroy()
        event.Skip()


    def onCloseWindow(self, event):
        # dialog to verify exit (including menuExit)
        dlg = wx.MessageDialog(self, "Want to exit?", "Exit", wx.YES_NO | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            self.OnClose(event)  # frame
        dlg.Destroy()


    def _open_file(self, path):
        self.filename, data = open_file(path)
        self.cases_panel.LoadCases(data)
        # Report on name of latest file read
        self.SetTitle("%s <%s>" % (self.title, self.filename))
        self.modified = False

    def FileOpen(self, event):
        dlg = wx.FileDialog(self, "Open Project", self.dirname, "", "*.gpc", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self._open_file(dlg.GetPath())
        dlg.Destroy()


#           Removed from menu until implementation

#    def EditCopy(self, event):
#        self.Info(self, 'This feature is not implemented yet')
#    def EditPaste(self, event):
#        self.Info(self, 'This feature is not implemented yet')
    def DataBox(self, num, event):
        self.Info(self, 'This feature is not implemented yet %s' % (num,))
    def Info(self, parent, message, caption = 'Not implemented yet'):
        dlg = wx.MessageDialog(parent, message, caption, \
            wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()


    def BuildMenu(self, menu):
        mainMenu = wx.MenuBar()
        for title, subMenu in menu:
            mainMenu.Append(self.BuildSubmenu(subMenu), title)
        return mainMenu

    def BuildSubmenu(self, subMenu):
        subMenuObject = wx.Menu()
        for item in subMenu:
            if not item:                #allow now to add separators
                subMenuObject.AppendSeparator()
                continue
            statustext = '';    uihandler = None
            if len(item) == 2:
                title, action = item
            elif len(item) == 3:
                if type(item[2]) is str:
                    title, action, statustext = item
                else:
                    title, action, statustext = item
            elif len(item) == 4:
                title, action, statustext, uihandler = item
            else:
                raise AssertionError, \
                    'Item %s should have either 2 to 4 parts' % (item,)
            if type(action) is list:
                _id = wx.NewId()
                subMenuObject.AppendMenu(_id, title, self.BuildSubmenu(action))
            else:
                _id = wx.NewId()
                subMenuObject.Append(_id, title, statustext)
                wx.EVT_MENU(self, _id, action)
            if uihandler:
                wx.EVT_UPDATE_UI(self, _id, uihandler)
        return subMenuObject





#!/usr/bin/env python
# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE, STDOUT
import os
import sys


def git_head():
    vers = 'dev'
    path = os.path.dirname(__file__)
    gitCmd = "git log HEAD --pretty=format:'%h (%ad)' --date=short -1"
    if 'win' in sys.platform:   # it's windows baby
        try:
            cmd = r'"C:\Program Files\Git\bin\sh.exe" --login -i'
            p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE,
                      stderr=STDOUT)
            output = p.communicate("cd ./projects/gpec/;" + gitCmd)[0]
            import re
            pattern = re.compile(r'(\w*[ \n]\(\d{4}-\d{2}-\d{2}\))')
            vers = pattern.findall(output)[0]
        except (IndexError, IOError):
            vers = 'dev'
    else:
        cmd = gitCmd
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE,
                  stderr=STDOUT, cwd=path)
        vers = p.stdout.read()
    return vers

_IN_DEV = False

# 3.x the current version
# 2.x (GPEC2010) was the Martin Gaitan's Computer Enginnering Final Project
# 1.x was Diego Nuñez's version
__status__ = 'Development' if _IN_DEV else 'Production'
__version__ = '3.2rc'
if _IN_DEV:
    __version__ += ' %s' % git_head()
__title__ = 'GPEC'
__full_title__ = "%s %s" % (__title__, __version__)
__author__ = [u'Martín Cismondi', u'Martín Gaitán']

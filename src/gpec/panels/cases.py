# -*- coding: utf-8 -*-
import os
import sys

import wx
import wx.lib.buttons
if __name__ == "__main__":
    from wx.lib.pubsub import setuparg1     # NOQA
from wx.lib.pubsub import pub
import wx.lib.scrolledpanel as scrolled
import wx.lib.agw.aui as aui
from wx.lib.buttons import GenBitmapTextButton
import gpec.ui.PyCollapsiblePane as pycp
from gpec.ui import widgets

from gpec import apimanager, crud
from gpec.ui.dialogs import RKPRDialog, KijDialog
from gpec.tools.misc import Counter, ch_val2pos
from gpec.settings import (Model, PATH_ICONS, EOS, EOS_SHORT, RKPR_MSG)


class CasePanel(scrolled.ScrolledPanel):

    def __init__(self, parent, id):
        scrolled.ScrolledPanel.__init__(self, parent, id,
                                        style=(wx.TAB_TRAVERSAL |
                                               wx.CLIP_CHILDREN |
                                               wx.FULL_REPAINT_ON_RESIZE))

        self.case_id = Counter().get_id()
        self.name = 'Case %i' % self.case_id

        self.api_manager = apimanager.ApiManager(self.case_id)
        self.box = wx.BoxSizer(wx.VERTICAL)
        self.model_options = EOS
        self.model_choice = wx.Choice(self, -1,
                                      choices=sorted(self.model_options.keys())
                                      )
        self.model_choice.Disable()

        #model ID by default
        self.model_id = 1
        position = ch_val2pos(self.model_choice, self.model_id)
        self.model_choice.SetSelection(position)
        first_row_sizer = wx.BoxSizer(wx.HORIZONTAL)
        try:
            bitmap = os.path.join(PATH_ICONS, "compose.png")
            self.load_button = GenBitmapTextButton(self, -1,
                                                   wx.Bitmap(bitmap), "Define system"
                                                   )
        except:
            raise Exception(bitmap)

        first_row_sizer.Add(self.load_button, 0, border=5,
                            flag=wx.ALL | wx.ALIGN_LEFT | wx.EXPAND)
        first_row_sizer.Add((10, 20), 0, wx.EXPAND)
        first_row_sizer.Add(wx.StaticText(self, -1, "Model:"), 0,
                            flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL, border=5)
        first_row_sizer.Add(self.model_choice, 0, border=5,
                            flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.box.Add(first_row_sizer, 0, border=5,
                     flag=wx.TOP | wx.LEFT | wx.FIXED_MINSIZE | wx.ALIGN_LEFT)
        self.panels = (VarsAndParamPanel(self, -1), VarsAndParamPanel(self, -1))
        self.box.Add(self.panels[0], 0, wx.EXPAND)
        self.box.Add(self.panels[1], 0, wx.EXPAND)

        #collapsible for extra variables

        if sys.platform == 'linux2':
            #on linux, if cp is an standard fixed panel, there is no space
            # for see the button at bottom

            self.cp = cp = pycp.PyCollapsiblePane(self, label='Other case variables',
                                                  style=(wx.CP_DEFAULT_STYLE |
                                                         wx.CP_NO_TLW_RESIZE |
                                                         pycp.CP_GTK_EXPANDER))
            self.MakeCollipsable(cp.GetPane())
            self.box.Add(self.cp, 0, wx.RIGHT | wx.LEFT | wx.BOTTOM, 10)
        else:
            #On Windows collapsible panel doesn't work very well.
            #It's not a time for stupid tasks
            self.cp = cp = wx.Panel(self, -1)
            self.MakeCollipsable(self.cp)
            tmp_box_sizer = wx.StaticBoxSizer(wx.StaticBox(self, -1,
                                              "Other case variables"), wx.VERTICAL)
            tmp_box_sizer.Add(self.cp, 0, wx.RIGHT | wx.LEFT | wx.EXPAND, 25)
            self.box.Add(tmp_box_sizer, 0, wx.RIGHT | wx.LEFT | wx.BOTTOM, 10)

        self.box.Add(wx.StaticLine(self), 0, wx.EXPAND)
        #BOTTOM. Diagram selector

        self.diag_hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.diagram_types = {0: 'Global Phase',
                              #1: 'Global Phase 3d',
                              1: 'Isopheths',
                              2: 'Pxy',
                              3: 'Txy'}

        self.diagram_ch = wx.Choice(self, -1, choices=[self.diagram_types[key]
                                                       for key in
                                                       sorted(self.diagram_types.keys())]
                                    )

        self.diagram_ch.SetSelection(0)
        st = wx.StaticText(self, -1, "Diagrams:", style=wx.ALIGN_RIGHT)
        self.diag_hbox.Add(st, 1, border=5,
                           flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)

        self.diag_hbox.Add(self.diagram_ch, 2, wx.EXPAND)
        self.box.Add(self.diag_hbox, 0, wx.ALL | wx.EXPAND, 5)
        #end diagram selector.
        plot_bitmap = wx.Bitmap(os.path.join(PATH_ICONS, "plot.png"))
        self.plot_button = GenBitmapTextButton(self, -1, plot_bitmap, "Plot!")

        but_sizer = wx.BoxSizer(wx.HORIZONTAL)
        but_sizer.Add(self.plot_button, 0, flag=wx.ALL, border=5)
        self.box.Add(but_sizer, 0, border=5,
                     flag=wx.ALL | wx.FIXED_MINSIZE | wx.ALIGN_RIGHT)
        self.SetSizerAndFit(self.box)
        self.SetClientSize(self.GetSize())
        self.SetupScrolling(scroll_x=False)

        #Binding
        self.Bind(wx.EVT_COLLAPSIBLEPANE_CHANGED, self.OnPaneChanged, cp)
        self.Bind(wx.EVT_CHOICE, self.OnSetModel, self.model_choice)
        self.Bind(wx.EVT_CHOICE, self.OnSetDiagram, self.diagram_ch)
        self.Bind(wx.EVT_BUTTON, self.OnLoadSystem, self.load_button)
        self.Bind(wx.EVT_BUTTON, self.OnMakePlots, self.plot_button)

        self.plots_history = []
        self.kij_data = None

    def OnSetDiagram(self, event):
        diagram_type_key = self.diagram_ch.GetSelection()

        try:
            for pos in (2, 2):      # after remove first 2, pos 3 turns into pos 2.
                item = self.diag_hbox.GetItem(pos)
                if item is not None and item.IsWindow():
                    self.diag_hbox.Remove(pos)
                    item.Show(False)
                    self.diag_hbox.Layout()
        except:
            # TO DO: "Errors should never pass silently."
            pass

        if diagram_type_key == 1:  # isopleth

            label = wx.StaticText(self, -1, "Z")  # for isopleths
            self.z_input = widgets.FloatCtrl(self, -1, "0.97")

            self.diag_hbox.Add(label, 0, border=5,
                               flag=wx.ALIGN_CENTER_VERTICAL | wx.ALL)
            self.diag_hbox.Add(self.z_input, 1, wx.EXPAND | wx.RIGHT, border=5)

        elif diagram_type_key == 2:  # pxy

            label = wx.StaticText(self, -1, "T [K]")      # for pxy
            self.t_input = widgets.FloatCtrl(self, -1, "300.0")

            self.diag_hbox.Add(label, 0, border=5,
                               flag=wx.ALIGN_CENTER_VERTICAL | wx.ALL)

            self.diag_hbox.Add(self.t_input, 1, border=5,
                               flag=wx.EXPAND | wx.EXPAND | wx.RIGHT)

        elif diagram_type_key == 3:
            label = wx.StaticText(self, -1, "P [bar]")     # for txy
            self.p_input = widgets.FloatCtrl(self, -1, "100.0")
            self.diag_hbox.Add(label, 0, border=5,
                               flag=wx.ALIGN_CENTER_VERTICAL | wx.ALL)
            self.diag_hbox.Add(self.p_input, 1, border=5,
                               flag=wx.EXPAND | wx.EXPAND | wx.RIGHT)

        self.diag_hbox.Layout()

    def _kij_init(self):
        # [[Kij as constant, [K', Kinf, T*]], selection]
        # K* default is the temperature of the lighter compound
        temperature = self.panels[0].GetVarsValues()[0]
        self.kij_data = [[0.0, [0.0, 0.0, temperature]], 0]

    def get_kij(self):
        """return a tuple (ntdep, value[s]). If ntdep is 0,
           values means a constant Kij"""

        ntdep = self.kij_data[-1]
        k12 = self.kij_data[0][ntdep]
        return ntdep, k12

    def set_kij(self, event):
        """
        defines wich policy to use for Kij
        could be:

        0: constant (default 0.0)
        1: temperature dependent :
            k12' (default is 0) k12infinite (default is 0)  T12* (default is Tc1)
        """

        if self.kij_data is None:
            self._kij_init()

        dlg = KijDialog(self, -1, "Set Kij", kij_data=self.kij_data)
        dlg.ShowModal()
        self.kij_data = dlg.kij_data
        dlg.Destroy()

    def MakeCollipsable(self, pane):
        addrSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)

        self.cr_options = {0: 'van Der Waals', 1: 'Lorentz-Berthelot'}
        choices = [self.cr_options[key] for key in sorted(self.cr_options.keys())]
        self.combining_rules = wx.Choice(pane, -1, choices=choices)

        self.combining_rules.SetSelection(0)
        combining_rulesLbl = wx.StaticText(pane, -1, "Combining Rule")
        self.max_p = widgets.FloatCtrl(pane, -1, "2000.0")
        max_pLbl = wx.StaticText(pane, -1, "Maximum Pressure for\n"
                                           "LL critical line  [bar]")

        k12Lbl = wx.StaticText(pane, -1, "Kij")

        # bitmap = os.path.join(PATH_ICONS, "compose.png")
        # self.k12 = GenBitmapTextButton(self, -1,
        #                                 wx.Bitmap(bitmap), "Kij"
        #                                            )

        self.k12 = wx.Button(pane, -1, u"Set",
                             style=wx.CENTRE | wx.BU_AUTODRAW)
        self.k12.SetToolTipString("Set Kij as constant or temperature dependent")
        self.k12.Disable()

        self.Bind(wx.EVT_BUTTON, self.set_kij, self.k12)

        l12Lbl = wx.StaticText(pane, -1, "Lij")
        self.l12 = widgets.FloatCtrl(pane, -1)

        addrSizer.Add(combining_rulesLbl, 0,
                      wx.ALIGN_RIGHT | wx.ALIGN_BOTTOM)
        addrSizer.Add(self.combining_rules, 0, wx.ALIGN_BOTTOM)

        addrSizer.Add(max_pLbl, 0,
                      wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.max_p, 0)

        addrSizer.Add(k12Lbl, 0,
                      wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.k12, -1)

        addrSizer.Add(l12Lbl, 0,
                      wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.l12, 0)

        border = wx.BoxSizer()
        border.Add(addrSizer, 1, wx.EXPAND | wx.ALL, 5)
        pane.SetSizer(border)

    def OnPaneChanged(self, evt=None):
        # redo the layout
        self.Layout()
        self.SetClientSize(self.GetSize())
        self.Fit()
        p = self.GetParent()
        #update auipane
        p.GetParent().GetParent()._mgr.Update()     # SetClientSize(self.GetSize())
        pub.sendMessage('refresh all', None)

    def SaveEssential(self):
        """Returns essential data"""

        compounds_data = [panel.GetData() for panel in self.panels]
        combining_rule = self.combining_rules.GetSelection()
        max_p = self.max_p.GetValue()
        lij = self.l12.GetValue()

        return {'compounds': compounds_data, 'case_id': self.case_id,
                'case_name': self.name, 'model_id': self.model_id,
                'combining_rule': combining_rule,
                'max_p': max_p,
                'lij': lij,
                'kij_data': self.kij_data,
                'history': self.plots_history}

    def LoadEssential(self, essential_data):
        """Load data to restore a case"""

        self.case_id = essential_data['case_id']
        self.name = essential_data['case_name']

        #it must be saved before set compounds
        self.OnSetModel(model_id=essential_data['model_id'])
        #TODO it would be better raise the event programatically

        #compounds
        for panel, data in zip(self.panels, essential_data['compounds']):
            panel.SetData(data)

        #extra (collapsible panel)
        self.combining_rules.SetSelection(essential_data['combining_rule'])

        self.max_p.SetValue(essential_data['max_p'])
        self.l12.SetValue(essential_data['lij'])
        self.kij_data = essential_data['kij_data']
        self.k12.Enable()

        self.plots_history = essential_data['history']
        self.Update()
        self.model_choice.Enable()
        self.Unbind(wx.EVT_BUTTON, self.load_button)
        self.OnChangeCloneButton()

    def ReplayHistory(self):
        """ iter over plots_history to reconstruct plots again """
        #TODO
        pass

    def OnChangeCloneButton(self):
        self.load_button.SetLabel('Clone case')
        self.load_button.SetBitmapLabel(wx.Bitmap(os.path.join(PATH_ICONS,
                                                               "edit-copy.png"))
                                        )
        self.Bind(wx.EVT_BUTTON, self.Clone, self.load_button)

    def OnLoadSystem(self, event=None):

        #GET DATA IF vars-paramPanels are enabled
        compounds_data = [panel.GetData() for panel in self.panels if panel.enabled]
        dlg = crud.DefineSystemDialog(None, -1, compounds_data)
        if dlg.ShowModal() == wx.ID_OK:
            for panel, data in zip(self.panels, compounds_data):
                data = data[:6]
                panel.SetData(data)
        self.model_choice.Enable()
        self.k12.Enable()
        dlg.Destroy()

    def OnSetModel(self, event=None, model_id=None):
        if model_id:
            self.flag_loading = True
            position = ch_val2pos(self.model_choice, model_id)
            self.model_choice.SetSelection(position)
        else:
            self.flag_loading = False

        new_model_id = model_id or self.model_options[event.GetString()]

        if self.model_id == new_model_id:
            #not EOS change. do nothing.
            return
        else:
            self.model_id = new_model_id

        self.direction = 0

        if self.model_id in (Model.PCSAFT, Model.SPHCT):
            #``PC-SAFT`` y ``SPHCT`` needs ``Lorentz-Berthelot`` combination rule.
            self.combining_rules.SetSelection(1)
            self.combining_rules.Disable()
        else:
            self.combining_rules.Enable()

        for panel in self.panels:
            panel.model_id = self.model_id
            panel.direction = self.direction
            panel.SetParamsForm(self.model_id)
            panel.SetDirectionOnForm()

        self.SetSizerAndFit(self.box)
        self.SetClientSize(self.GetSize())

    def is_bounded(self):
        """returns True if the system was defined for the case"""
        return all([p.GetVarsValues() for p in self.panels])

    def FreezeAll(self):
        """Freeze the case"""

        self.model_choice.Disable()
        self.panels[0].EnableAll(freeze=True)
        self.panels[1].EnableAll(freeze=True)
        self.model_options
        self.combining_rules.Disable()
        self.max_p.Disable()
        self.k12.Disable()
        self.l12.Disable()

        self.Unbind(wx.EVT_BUTTON, self.load_button)
        self.OnChangeCloneButton()

    def Clone(self, event):
        """get data a send a message to TabbedCases to add
           a new case with the same parameters"""
        data = self.SaveEssential()
        pub.sendMessage('clone case', data)

    def GetSystem(self):
        """return a list with the compounds name of the system"""
        extra = [EOS_SHORT[self.model_id], self.get_kij(), self.l12.GetValue()]
        return [c.compound_name for c in self.panels] + extra

    def OnMakePlots(self, event):
        if self.panels[0].setup_data and self.panels[1].setup_data:
            #case is defined
            comp1 = self.panels[0].GetTotalData()
            comp2 = self.panels[1].GetTotalData()
            if not comp1 or not comp2:
                #if something fails on model param calcultions => aborted
                return

            if len(self.plots_history) == 0:
                #if it's first set of diagrams, freeze the case.
                self.FreezeAll()

            ncomb = self.combining_rules.GetSelection()

            if not self.kij_data:
                self._kij_init()

            ntdep, k12 = self.get_kij()
            l12 = self.l12.GetValue()
            max_p = self.max_p.GetValue()

            curves = self.api_manager.gpecin2gpecout(self.model_id, comp1,
                                                     comp2, ncomb, ntdep, k12,
                                                     l12, max_p)

            diagram_selection = self.diagram_ch.GetSelection()

            if diagram_selection == 0:  # global
                pub.sendMessage('make.globalsuite', (self.case_id, self.name, curves,
                                                     self.GetSystem()))
                self.plots_history.append(('globalsuite'))

            elif diagram_selection == 1:  # isopleth
                z = self.z_input.GetValue()
                self.api_manager.write_generic_inparam('z', z)
                curves_isop = self.api_manager.read_generic_output('isop')
                pub.sendMessage('make.isop', (self.case_id, self.name, curves_isop, z,
                                              self.GetSystem()))
                self.plots_history.append(('isop', z))

            elif diagram_selection == 2:  # pxy
                t = self.t_input.GetValue()
                self.api_manager.write_generic_inparam('t', t)
                curves_pxy = self.api_manager.read_generic_output('pxy')
                pub.sendMessage('make.pxy', (self.case_id, self.name, curves_pxy, t,
                                             self.GetSystem()))
                self.plots_history.append(('pxy', t))

            elif diagram_selection == 3:  # txy
                p = self.p_input.GetValue()
                self.api_manager.write_generic_inparam('p', p)
                curves_txy = self.api_manager.read_generic_output('txy')
                pub.sendMessage('make.txy', (self.case_id, self.name,
                                             curves_txy, p, self.GetSystem()))
                self.plots_history.append(('txy', p))
        else:
            pub.sendMessage('log',
                            ('error', "Nothing to calculate. Define the system first."))


class TabbedCases(wx.Panel):

    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id)
        self.nb = aui.AuiNotebook(self, style=aui.AUI_NB_TOP,
                                  agwStyle=aui.AUI_NB_CLOSE_ON_ACTIVE_TAB)
        self.AddNewCase(0)      # a first one case
        self.create_new_case = True
        self.AddNewCaseButton()     # the last `+` page
        self.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.onPageChange, self.nb)
        self.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.onPageClose, self.nb)
        sizer = wx.BoxSizer()
        sizer.Add(self.nb, 1, wx.EXPAND)
        self.SetSizerAndFit(sizer)
        size = self.nb.GetPage(0).GetSize()
        self.SetSize(size)
        self.veto = False       # in order to desactivate add new page on page change.
        pub.subscribe(self.CloneCase, 'clone case')

    def CloneCase(self, message):
        """receive a message from a panel, with its essential data.
           this method create a new case and load that data
           taking care about its name and id, to get a clone with other identity"""
        data = message.data
        clone = self.AddNewCase()
        id_bak = clone.case_id
        name_bak = clone.name
        clone.LoadEssential(data)
        clone.case_id = id_bak
        clone.name = name_bak
        clone.plots_history = []
        self.UpdatePagesTitle()

    def AddNewCaseButton(self):
        ico = os.path.join(PATH_ICONS, 'add.png')
        self.dummy = wx.Panel(self, -1)      # dummy Panel
        self.nb.AddPage(self.dummy, "", bitmap=wx.Bitmap(ico, wx.BITMAP_TYPE_PNG))

    def onPageClose(self, evt):
        if self.nb.GetPageCount() != 2:
            self.create_new_case = False
        evt.Veto()
        page_selected = self.nb.GetSelection()
        if evt.GetSelection() + 2 == self.nb.GetPageCount():    # last tab selected
            wx.CallAfter(self.nb.SetSelection, self.nb.GetPageCount() - 3)
        self.nb.RemovePage(page_selected)

    def SaveCases(self):
        cases = [page.SaveEssential() for page in
                 map(self.nb.GetPage, range(self.nb.GetPageCount() - 1))]
        return cases

    def LoadCases(self, cases_data):
        self.veto = True

        #delete all first
        for idx in range(self.nb.GetPageCount() - 1):
            case = self.nb.GetPage(idx)
            self.nb.RemovePage(idx)
            case.Destroy()

        for idx, case_data in enumerate(cases_data):
            case = self.AddNewCase(idx)    # idx
            case.LoadEssential(case_data)

        #self.AddNewCaseButton()
        self.UpdatePagesTitle()
        self.veto = False
        pub.sendMessage('refresh all', None)

    def UpdatePagesTitle(self):
        for idx in range(self.nb.GetPageCount() - 1):
            case_panel = self.nb.GetPage(idx)
            self.nb.SetPageText(idx, case_panel.name)

    def onPageChange(self, evt):
        # last tab selected?
        if evt.GetSelection() + 1 == self.nb.GetPageCount() and self.create_new_case:
            self.AddNewCase(evt.GetSelection())
        self.create_new_case = True

    def AddNewCase(self, location=-1):
        """ create a new case tab at location or at the last
            position if location is not given"""

        if location == -1:
            location = self.nb.GetPageCount() - 1

        case = CasePanel(self, -1)
        self.nb.InsertPage(location, case, "Case %i" % case.case_id)
        wx.CallAfter(self.nb.SetSelection, location)
        return case     # useful to chain


class VarsAndParamPanel(wx.Panel):
    """a panel with 2 columns of inputs. First colums input EOS variables.
        The second one are the inputs to model parameters (change depending
        the model selected).
        This parameter are related and is possible to calcule a group of values
        defining the other one.
        """

    def __init__(self, parent, id, model_id=1, setup_data=None):
        """setup_data: (id, name, tc, pc, vc, om, rkpr_data)"""

        wx.Panel.__init__(self, parent, id,
                          style=(wx.TAB_TRAVERSAL
                                 | wx.CLIP_CHILDREN
                                 | wx.FULL_REPAINT_ON_RESIZE)
                          )

        gbs = self.gbs = widgets.GridBagSizerEnh(6, 5)

        self.api_manager = parent.api_manager

        self.vars_label = (('Tc [K]', 'Critical temperature'),
                           ('Pc [bar]', 'Critical Pressure'),
                           ('Vc [l/mol]', 'Critical Volume'),
                           (u'\u03c9', 'Acentric Factor'))

        # add title
        self.title = wx.StaticText(self, -1, '', (5, 120), style=wx.ALIGN_LEFT)
        self.title.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.NORMAL))
        gbs.Add(self.title, (0, 0), (1, 4), flag=wx.ALIGN_CENTER)
        #add first col

        self.vars = []
        for row, var in enumerate(self.vars_label):
            gbs.Add(wx.StaticText(self, -1, var[0]), (row + 2, 0), flag=wx.ALIGN_RIGHT)
            box = widgets.FloatCtrl(self, -1)
            box.SetToolTipString(var[1])
            self.vars.append(box)
            gbs.Add(box, (row + 2, 1))
        self.params = []
        #add radio buttons
        self.radio1 = wx.RadioButton(self, -1, "", style=wx.RB_GROUP)
        self.radio2 = wx.RadioButton(self, -1, "")
        self.gbs.Add(self.radio1, (1, 1))
        self.gbs.Add(self.radio2, (1, 4))
        labels = {Model.SRK: ((u'ac [bar·m\u2076Kmol\u00B2]',
                               u'Critical value for the attractive parameter'),
                              (u'b [l/mol]',
                               u'Repulsive parameter in SRK'),
                             (u'm',
                              u'Parameter for temperature dependence of '
                              u'the attractive parameter')),
                  Model.PR: ((u'ac [bar·m^6Kmol\u00B2]',
                              u'Critical value for the attractive parameter'),
                             (u'b [l/mol]',
                              u'Temperature dependence of the attractive parameter'),
                             (u'm',
                              u'Parameter for the temperature dependence of the '
                              u'attractive parameter')),
                  Model.RKPR: ((u'ac [bar·m\u2076Kmol\u00B2]',
                                u'Critical value for the attractive parameter'),
                               (u'b [l/mol]', u'Repulsive parameter in RK-PR'),
                               (u'\u03b4\u2081', 'RK-PR third parameter'),
                               (u'k',
                                u'Parameter for the temperature dependence of '
                                u'the attractive parameter')),
                  Model.PCSAFT: ((u'\u03b5/k',
                                  u'Depth of square well potential '
                                  u'(PC-SAFT attractive parameter)'),
                                (u'\u03C3',
                                 u'Segment diameter (repulsive parameter in PC-SAFT)'),
                                (u'm',
                                 u'Number of segments (PC-SAFT third parameter)')),
                  Model.SPHCT: ((u'T* [k]',
                                 u'Attractive parameter in SPHCT'),
                                (u'V* [l/mol]', 'Repulsive parameter in SPHCT'),
                                (u'c', u'External rotational and vibrational degrees '
                                       u'of freedom (SPHCT third parameter)'))
                  }
        self.params_labels = labels

        #add button in the center
        self.arrow = (wx.Bitmap(os.path.join(PATH_ICONS, "go-next.png")),
                      wx.Bitmap(os.path.join(PATH_ICONS, "go-previous.png")))
        self.button = wx.BitmapButton(self, -1, self.arrow[0],
                                      style=wx.CENTRE | wx.BU_AUTODRAW)

        gbs.Add(self.button, (3, 2), flag=wx.FIXED_MINSIZE | wx.ALIGN_CENTER)

        # Add a spacer at the end to ensure some extra space at the bottom
        #gbs.Add((10,10), (14,7))

        self.box = wx.BoxSizer()
        self.box.Add(gbs, 0, wx.ALL, 10)

        #set default on form
        self.direction = 0
        self.model_id = model_id
        self.setup_data = setup_data
        if setup_data is not None:
            self.enabled = True
            self.SetData(setup_data)
            self.SetDirectionOnForm()
        else:
            self.EnableAll(False)
        self.SetParamsForm(self.model_id)
        self.rkpr_data = None

        #binding
        self.Bind(wx.EVT_BUTTON, self.OnButton, self.button)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnDirectionSelect, self.radio1)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnDirectionSelect, self.radio2)

    def SetData(self, data):
        """Set basic compound data on the panel"""
        if data:

            if isinstance(data[-1], dict):
                #necessary to work width thre original value.
                #this need a lot of testing
                self.vc_back = data[-1]['vc_back']
                if 'vc_ratio' in data[-1]:
                    self.vc_ratio = data[-1]['vc_ratio']
                self.rkpr_data = data[-1].get('rkpr_data', None)
                self.direction = data[-1].get('direction', 0)
                if 'params' in data[-1]:
                    self.SetParamsValues(data[-1]['params'])

                data[4] = self.vc_back
                data = data[:-1]
            else:
                #data come from database
                self.vc_back = data[4]  # save original value
                self.rkpr_data = None
            self.setup_data = data
            self.compound_id = int(data[0])
            self.compound_name = data[1]    # in case the title != compound_name
            self.title.SetLabel(data[1])
            self.SetVarsValues(data[2:])

            #TODO reset param columns.
            self.EnableAll()
            self.SetDirectionOnForm()

    def GetData(self):
        """Return a compound list [id, name, vars...]   useful to redefine the system"""
        if self.setup_data:
            r = [self.compound_id, self.compound_name] + self.GetVarsValues()
            vc_back = getattr(self, 'vc_back', float(r[-2]))

            _extra = {'vc_back': vc_back,
                      'rkpr_data': self.rkpr_data,
                      'direction': self.direction,
                      'params': self.GetParamsValues()}

            if hasattr(self, 'vc_ratio'):
                _extra['vc_ratio'] = self.vc_ratio
            return r + [_extra]

    def GetTotalData(self):
        """Return a compound list tuple (name, (vars...), (param...) )"""
        #Ensure last numbers generating (as a programatical event)

        if not hasattr(self, 'conparout_ok'):
            self.OnButton(None)

        if self.conparout_ok:
            tmp_var = self.GetVarsValues()
            tmp_var = tmp_var[:-2] + [tmp_var[-1], tmp_var[-2]]
            # gpecout has order changed: vc <-> omega
            if self.model_id == Model.RKPR:
                tmp_var.append(self.vc_ratio)
            return (self.compound_name, tmp_var, self.GetParamsValues())
        else:
            return False

    def EnableAll(self, flag=True, freeze=False):
        """
        enable or disable (defined by flag) the inputs of this panel.
        if 'freeze' mode is on, it's disable all in fact, but
        self.enabled is set to True. (so, user couldn't edit, but
        still be able to plot.
        """
        if freeze:
            flag = False
            self.enabled = True
        else:
            self.enabled = flag
        self.enabled = flag
        self.radio1.Enable(flag)
        self.radio2.Enable(flag)
        for box in self.vars:
            box.Enable(flag)
        self.vars[2].Enable(False)
        self.button.Enable(flag)

    def GetVarsValues(self):
        """Return vars values of defined compound"""
        if self.setup_data:
            return [float(box.GetValue()) for box in self.vars]

    def SetVarsValues(self, data):
        try:
            for box, data_c in zip(self.vars, data):
                #on PC-SAFT and SPHCT om is 0.0 in CONPAOUT and should be ignored.
                if float(data_c) != 0.0:
                    box.SetValue(str(data_c))
        except:
            pub.sendMessage('log',
                            ('error', "not enough data or boxes for compounds vars"))

    def GetParamsValues(self):
        """Return params values of defined compound"""
        if self.setup_data:
            return [box.GetValue() for box in self.params]

    def SetParamsValues(self, data):
        if len(data) == len(self.params):
            for box, data in zip(self.params, data):
                box.SetValue(str(data))
        else:
            pub.sendMessage('log', ('error',
                                    "not enough data or boxes for EOS parameter"))

    def OnDirectionSelect(self, event):
        radio_selected = event.GetEventObject()
        if radio_selected == self.radio1:
            self.direction = 0
        else:
            self.direction = 1
        self.SetDirectionOnForm()

    def SetDirectionOnForm(self):
        """Update the form. Direction and enable of"""
        if self.direction == 0:
            self.radio1.SetValue(True)
            for box in self.vars:
                box.Enable(True)
            #if self.model_id != 3:
            self.vars[2].Enable(False)
            for box in self.params:
                box.Enable(False)

            self.button.SetBitmapLabel(self.arrow[0])
        else:
            self.radio2.SetValue(True)
            for box in self.vars:
                box.Enable(False)
            for box in self.params:
                box.Enable(True)
            self.button.SetBitmapLabel(self.arrow[1])

    def OnButton(self, event):
        self.conparout_ok = False

        if self.direction == 0:
            if self.model_id == Model.RKPR:
                self.set_rkpr_policy()
            data_in = [box.GetValue() for box in self.vars]
        else:
            data_in = [box.GetValue() for box in self.params]

        rkpr_in_data = getattr(self, 'rkpr_in', None)
        data = self.api_manager.conparin2conparout(self.direction,
                                                   self.model_id,
                                                   data_in,
                                                   rkpr_in_data)
        try:
            #update vc_ratio
            self.vc_ratio = float(data[0][2]) / float(self.GetVarsValues()[2])
            self.SetVarsValues(data[0])
            self.SetParamsValues(data[1])

            self.conparout_ok = True
        except:
            pub.sendMessage('log', ("error", "Error handling ModelsParam output. "
                                             "Upcoming calculations aborted."))

    def SetParamsForm(self, model_id):
        """set a column of widgets for params depending on selected model"""
        #clean up
        self.model_id = model_id

        # setup a button for RKPR dialog
        if model_id == Model.RKPR:
            widget = wx.Button(self, -1, u" \u03C1 ",
                               style=wx.CENTRE | wx.BU_AUTODRAW)
            widget.SetToolTipString(RKPR_MSG)
            self.Bind(wx.EVT_BUTTON, self.set_vc_rat, widget)
            self.gbs.Add(widget, (4, 2), flag=wx.FIXED_MINSIZE | wx.ALIGN_CENTER)
        else:
            self.gbs.remove_gbitem(4, 2)

        for row in range(len(self.params)):
            self.gbs.remove_gbitem(row + 2, 3)
            self.gbs.remove_gbitem(row + 2, 4)
        self.params = []
        for row, var in enumerate(self.params_labels[model_id]):
            #add row an box to the form and the list
            self.gbs.Add(wx.StaticText(self, -1, var[0]), (row + 2, 3),
                         flag=wx.ALIGN_RIGHT)
            box = widgets.FloatCtrl(self, -1)
            box.SetToolTipString(var[1])
            self.params.append(box)
            self.gbs.Add(box, (row + 2, 4))
        if self.direction == 0:
            self.SetDirectionOnForm()
        self.EnableAll(self.enabled)

        #FIT all
        self.gbs.Layout()
        self.SetSizerAndFit(self.box)
        self.SetClientSize(self.GetSize())

    def _rkpr_extra_init(self):
        """
        this is the initialization for Rkpr extra dialog. Just called once
        The structure is this:
                      [[Zr, RhoLsat at T, RhoLsat a Tr], selected]
        where `selected` is 0, 1 or 2 for the option selected. By default it's 2
        """
        temperature = self.GetVarsValues()[0]
        self.rkpr_data = [[1.168, 0.7 * temperature, 0.7], 2]

    def set_vc_rat(self, event):
        """
        on RKPR open a dialog box and associate the input value as the vc_rat
        """
        if self.rkpr_data is None:
            self._rkpr_extra_init()

        if self.model_id == Model.RKPR:
            dlg = RKPRDialog(self, -1, self.title.GetLabel(), extra_data=self.rkpr_data)
            dlg.ShowModal()
            self.rkpr_data = dlg.extra_data

        elif hasattr(panel, 'vc_back'):
            # TO DO revisar!
            values = self.GetVarsValues()
            values[2] = self.vc_back
            self.SetVarsValues(values)

    def set_rkpr_policy(self):
        """
        defines wich policy to use to as rkpr extra parameter
        it could be

        0: Zrat (default: 1.168),
        1: RhoLSat at T (default: 0.7 * Tc),
        2: RhoLSat at Tr (default: 0.7)
        """

        def formula(cid, T):
            # retrieve parameters
            conn, cu = crud.start_db()
            query = "select A, B, C, D from compounds where id=?"
            A, B, C, D = cu.execute(query, (cid,)).fetchone()
            conn.close()
            rholsat = A / B ** (1 + (1 - T / C) ** D)
            return rholsat

        if self.rkpr_data is None:
            self._rkpr_extra_init()

        values = self.GetVarsValues()

        policy = self.rkpr_data[1]
        policy_value = self.rkpr_data[0][policy]

        if policy == 0:
            # clean old value if exist
            if hasattr(self, 'rkpr_in'):
                del self.rkpr_in

            self.vc_ratio = policy_value

            if values:
                self.vc_back = values[2]
                values[2] = float(values[2]) * self.vc_ratio
                self.SetVarsValues(values)
        elif policy == 1:
            T = policy_value
            self.rkpr_in = [T, formula(self.compound_id, T)]
        elif policy == 2:
            tc = values[0]
            T = tc * policy_value
            self.rkpr_in = [T, formula(self.compound_id, T)]


if __name__ == '__main__':

    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    frame = wx.Frame(None)
    panel = TabbedCases(frame, -1)
    if len(sys.argv) > 1:
        from gpec.ui.helpers import open_file
        _, data = open_file(sys.argv[1])
        panel.LoadCases(data)
    frame.Centre()
    frame.Fit()
    frame.Show(True)
    app.MainLoop()
